<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('mpost');
    }

    public function index()
    {
        /*if(!IsLogin()) {
            redirect('user/login');
        }
        redirect('user/dashboard');*/
        $data['title'] = 'Beranda';
        $data['news'] = $this->mpost->search(5,"",1);
        $data['gallery'] = $this->mpost->search(5,"",4);
        $this->load->view('home/index', $data);
    }

    public function index_2() {
        $data['title'] = 'Beranda';
        $data['news'] = $this->mpost->search(4,"",1);
        $data['gallery'] = $this->mpost->search(4,"",4);
        $this->load->view('home/index_2', $data);
    }

    function _404() {
        $this->load->view('home/error');
    }
}
