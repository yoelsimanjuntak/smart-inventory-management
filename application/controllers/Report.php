<?php
/**
 * Created by PhpStorm.
 * User: Toshiba
 * Date: 6/15/2019
 * Time: 12:34 PM
 */
class Report extends MY_Controller {
    function __construct() {
        parent::__construct();
        if(!IsLogin()) {
            redirect('user/login');
        }
    }

    function receipt() {
        $data['title'] = "Laporan Barang Masuk";
        $data['filter'] = $filter = array(
            COL_DEPARTMENTID => $this->input->get(COL_DEPARTMENTID),
            COL_LOCATIONID => $this->input->get(COL_LOCATIONID),
            COL_STOCKID => $this->input->get(COL_STOCKID),
            COL_PURCHASENO => $this->input->get(COL_PURCHASENO),
            "DateFrom" => $this->input->get("DateFrom") ? $this->input->get("DateFrom") : date("Y-m").'-01',
            "DateTo" => $this->input->get("DateTo") ? $this->input->get("DateTo") : date("Y-m-d")
        );
        $data['res'] = array();
        if(!empty($_GET)) {
            $q = $this->db
                ->select(TBL_MDEPARTMENT.'.'.COL_DEPARTMENTNAME.','.TBL_MLOCATION.'.'.COL_LOCATIONNAME.','.TBL_MSTOCK.'.'.COL_STOCKNAME.','.TBL_MSATUAN.'.'.COL_SATUANNAME.','.TBL_STOCKPURCHASES.".*, CONCAT(LPAD(mstock.CategoryID, 3, '0'),'.',LPAD(stockpurchases.StockID, 3, '0'),'.',LPAD(stockpurchases.PurchaseID, 4, '0'),'.',YEAR(stockpurchases.PurchaseDate)) AS PurchaseNo_")
                ->join(TBL_MSTOCK,TBL_MSTOCK.'.'.COL_STOCKID." = ".TBL_STOCKPURCHASES.".".COL_STOCKID,"left")
                ->join(TBL_MSATUAN,TBL_MSATUAN.'.'.COL_SATUANID." = ".TBL_MSTOCK.".".COL_SATUANID,"left")
                ->join(TBL_MDEPARTMENT,TBL_MDEPARTMENT.'.'.COL_DEPARTMENTID." = ".TBL_STOCKPURCHASES.".".COL_DEPARTMENTID,"left")
                ->join(TBL_MLOCATION,TBL_MLOCATION.'.'.COL_LOCATIONID." = ".TBL_STOCKPURCHASES.".".COL_LOCATIONID,"left");
            if(!empty($filter[COL_DEPARTMENTID])) $q->where(TBL_STOCKPURCHASES.'.'.COL_DEPARTMENTID, $filter[COL_DEPARTMENTID]);
            if(!empty($filter[COL_LOCATIONID])) $q->where(TBL_STOCKPURCHASES.'.'.COL_LOCATIONID, $filter[COL_LOCATIONID]);
            if(!empty($filter[COL_STOCKID])) $q->where(TBL_STOCKPURCHASES.'.'.COL_STOCKID, $filter[COL_STOCKID]);
            if(!empty($filter[COL_PURCHASENO])) {
              $this->db->like("CONCAT(LPAD(mstock.CategoryID, 3, '0'),'.',LPAD(stockpurchases.StockID, 3, '0'),'.',LPAD(stockpurchases.PurchaseID, 4, '0'),'.',YEAR(stockpurchases.PurchaseDate))", $filter[COL_PURCHASENO]);
            }
            if(!empty($filter["DateFrom"])) $q->where(TBL_STOCKPURCHASES.'.'.COL_PURCHASEDATE." >= ", $filter["DateFrom"]);
            if(!empty($filter["DateTo"])) $q->where(TBL_STOCKPURCHASES.'.'.COL_PURCHASEDATE." <= ", $filter["DateTo"]);

            $data['res'] = $q->get(TBL_STOCKPURCHASES)->result_array();
        }

        $this->load->view('report/receipt', $data);
    }

    function item_per_location() {
        $data['title'] = "Laporan Barang per Lokasi";
        $data['filter'] = $filter = array(
            COL_DEPARTMENTID => $this->input->get(COL_DEPARTMENTID),
            COL_LOCATIONID => $this->input->get(COL_LOCATIONID),
            COL_STOCKID => $this->input->get(COL_STOCKID),
            COL_PURCHASENO => $this->input->get(COL_PURCHASENO),
            COL_DATE => $this->input->get(COL_DATE) ? $this->input->get(COL_DATE) : date("Y-m-t"),
            COL_CONDITIONID => $this->input->get(COL_CONDITIONID),
        );
        $data['res'] = array();
        if(!empty($_GET)) {
            $q = @"
            SELECT
tbl.*,
loc.LocationName,
st.StockName,
sat.SatuanName,
con.ConditionName
FROM (
	SELECT
	pur.DepartmentID,
	pur.StockID,
	pur.PurchaseNo,
	pur.PurchaseID,
  pur.PurchaseDate,
  pur.BudgetYear,
	issue.IssueID,
	i.ItemID,
	(
		SELECT COALESCE(t_.DestinationID, si.LocationID) AS LastLocationID FROM stockmovements mvt
		LEFT JOIN stockissues si ON si.IssueID = mvt.IssueID
		LEFT JOIN stockissueitems i_ ON i_.IssueID = mvt.IssueID
		LEFT JOIN stocktransfers t_ ON t_.TransferID = mvt.TransferID
		WHERE
			mvt.StockID = pur.StockID
			AND i_.ItemID  = i.ItemID
			AND COALESCE(t_.ItemID, i_.ItemID) = i.ItemID
			AND mvt.Date <= ?
		ORDER BY
			mvt.StockMovementType DESC,
			mvt.Date DESC,
			mvt.ID DESC
		LIMIT 1
	) AS LastLocationID,
  (
      SELECT COALESCE(t_.Condition, pur.Condition) AS LastConditionID FROM stockmovements mvt
      LEFT JOIN stockissues si ON si.IssueID = mvt.IssueID
  		LEFT JOIN stockissueitems i_ ON i_.IssueID = mvt.IssueID
  		LEFT JOIN stocktransfers t_ ON t_.TransferID = mvt.TransferID
      WHERE
          mvt.StockID = pur.StockID
          AND i_.ItemID  = i.ItemID
          AND COALESCE(t_.ItemID, i_.ItemID) = i.ItemID
          AND mvt.Date <= ?
      ORDER BY
          mvt.StockMovementType DESC,
          mvt.Date DESC,
          mvt.ID DESC
      LIMIT 1
  ) AS LastConditionID,
	(
		SELECT mvt.Date FROM stockmovements mvt
		LEFT JOIN stockissues si ON si.IssueID = mvt.IssueID
		LEFT JOIN stockissueitems i_ ON i_.IssueID = mvt.IssueID
		LEFT JOIN stocktransfers t_ ON t_.TransferID = mvt.TransferID
		WHERE
			mvt.StockID = pur.StockID
			AND i_.ItemID  = i.ItemID
			AND COALESCE(t_.ItemID, i_.ItemID) = i.ItemID
			AND mvt.Date <= ?
		ORDER BY
			mvt.StockMovementType DESC,
			mvt.Date DESC,
			mvt.ID DESC
		LIMIT 1
	) AS LastMovementDate,
  CONCAT(
      LPAD(s.CategoryID, 3, '0'),'.',
      LPAD(pur.StockID, 3, '0'),'.',
      LPAD(pur.PurchaseID, 4, '0'),'.',
      YEAR(pur.PurchaseDate)
  ) AS PurchaseNo_
	FROM stockpurchases pur
  INNER JOIN mstock s ON s.StockID = pur.StockID
	INNER JOIN stockissues issue ON issue.PurchaseID = pur.PurchaseID
	INNER JOIN stockissueitems i ON i.IssueID = issue.IssueID
	WHERE
		pur.DepartmentID = ?
) tbl
LEFT JOIN mlocation loc ON loc.LocationID = tbl.LastLocationID
LEFT JOIN mcondition con ON con.ConditionID = tbl.LastConditionID
LEFT JOIN mstock st ON st.StockID = tbl.StockID
LEFT JOIN msatuan sat ON sat.SatuanID = st.SatuanID
WHERE 1=1";
            if($filter[COL_LOCATIONID]) {
              $q .= " AND tbl.LastLocationID = ".$filter[COL_LOCATIONID];
            }
            if($filter[COL_CONDITIONID]) {
              $q .= " AND tbl.LastConditionID = ".$filter[COL_CONDITIONID];
            }
            $q .= " ORDER BY tbl.LastMovementDate DESC ";

            $data['res'] = $this->db->query($q, array($filter[COL_DATE], $filter[COL_DATE], $filter[COL_DATE], $filter[COL_DEPARTMENTID]))->result_array();
        }
        $this->load->view('report/item_per_location', $data);
    }
}
