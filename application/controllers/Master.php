<?php
/**
 * Created by PhpStorm.
 * User: Toshiba
 * Date: 6/14/2019
 * Time: 8:09 AM
 */
class Master extends MY_Controller {
    function __construct() {
        parent::__construct();
        if(!IsLogin()) {
            redirect('user/login');
        }
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }
    }

    function location_index() {
        $data['title'] = "Lokasi";
        $data['res'] = $this->db->get(TBL_MLOCATION)->result_array();
        $this->load->view('master/location_index', $data);
    }

    function location_add() {
        $user = GetLoggedUser();
        $data['title'] = "Lokasi";
        $data['edit'] = FALSE;

        if(!empty($_POST)){
            $data['data'] = $_POST;
            $data = array(
                COL_LOCATIONNAME => $this->input->post(COL_LOCATIONNAME),
                COL_DESCRIPTION => $this->input->post(COL_DESCRIPTION),
                COL_LATITUDE => $this->input->post(COL_LATITUDE),
                COL_LONGITUDE => $this->input->post(COL_LONGITUDE),
                COL_CREATEDBY => $user[COL_USERNAME],
                COL_CREATEDON => date('Y-m-d H:i:s')
            );
            $res = $this->db->insert(TBL_MLOCATION, $data);
            if($res) {
                redirect('master/location-index');
            } else {
                redirect(current_url()."?error=1");
            }
        }
        else {
            $this->load->view('master/location_form', $data);
        }
    }

    function location_edit($id) {
        $user = GetLoggedUser();
        $data['title'] = "Lokasi";
        $data['edit'] = TRUE;
        $data['data'] = $edited = $this->db->where(COL_LOCATIONID, $id)->get(TBL_MLOCATION)->row_array();
        if(empty($edited)){
            show_404();
            return;
        }

        if(!empty($_POST)){
            $data['data'] = $_POST;
            $data = array(
                COL_LOCATIONNAME => $this->input->post(COL_LOCATIONNAME),
                COL_DESCRIPTION => $this->input->post(COL_DESCRIPTION),
                COL_LATITUDE => $this->input->post(COL_LATITUDE),
                COL_LONGITUDE => $this->input->post(COL_LONGITUDE),
                COL_UPDATEDBY => $user[COL_USERNAME],
                COL_UPDATEDON => date('Y-m-d H:i:s')
            );
            $res = $this->db->where(COL_LOCATIONID, $id)->update(TBL_MLOCATION, $data);
            if($res) {
                redirect('master/location-index');
            } else {
                redirect(current_url()."?error=1");
            }
        }
        else {
            $this->load->view('master/location_form', $data);
        }
    }

    function location_delete(){
        if(!IsLogin() || GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }
        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_MLOCATION, array(COL_LOCATIONID => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }

    function department_index() {
        $data['title'] = "Instansi";
        $data['res'] = $this->db->get(TBL_MDEPARTMENT)->result_array();
        $this->load->view('master/department_index', $data);
    }

    function department_add() {
        $user = GetLoggedUser();
        if(!empty($_POST)){
            $data['data'] = $_POST;
            $data = array(
                COL_DEPARTMENTNAME => $this->input->post(COL_DEPARTMENTNAME),
                COL_CREATEDBY => $user[COL_USERNAME],
                COL_CREATEDON => date('Y-m-d H:i:s')
            );

            $res = $this->db->insert(TBL_MDEPARTMENT, $data);
            if($res) {
                ShowJsonSuccess("Berhasil");
            } else {
                ShowJsonError("Gagal");
            }
        }
    }

    function department_edit($id) {
        $user = GetLoggedUser();
        if(!empty($_POST)){
            $data['data'] = $_POST;
            $data = array(
                COL_DEPARTMENTNAME => $this->input->post(COL_DEPARTMENTNAME),
                COL_UPDATEDBY => $user[COL_USERNAME],
                COL_UPDATEDON => date('Y-m-d H:i:s')
            );

            $res = $this->db->where(COL_DEPARTMENTID, $id)->update(TBL_MDEPARTMENT, $data);
            if($res) {
                ShowJsonSuccess("Berhasil");
            } else {
                ShowJsonError("Gagal");
            }
        }
    }

    function department_delete(){
        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_MDEPARTMENT, array(COL_DEPARTMENTID => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }

    function category_index() {
        $data['title'] = "Kategori Barang";
        $data['res'] = $this->db->get(TBL_MCATEGORY)->result_array();
        $this->load->view('master/category_index', $data);
    }

    function category_add() {
        $user = GetLoggedUser();
        if(!empty($_POST)){
            $data['data'] = $_POST;
            $data = array(
                COL_CATEGORYNAME => $this->input->post(COL_CATEGORYNAME),
                COL_CREATEDBY => $user[COL_USERNAME],
                COL_CREATEDON => date('Y-m-d H:i:s')
            );

            $res = $this->db->insert(TBL_MCATEGORY, $data);
            if($res) {
                ShowJsonSuccess("Berhasil");
            } else {
                ShowJsonError("Gagal");
            }
        }
    }

    function category_edit($id) {
        $user = GetLoggedUser();
        if(!empty($_POST)){
            $data['data'] = $_POST;
            $data = array(
                COL_CATEGORYNAME => $this->input->post(COL_CATEGORYNAME),
                COL_UPDATEDBY => $user[COL_USERNAME],
                COL_UPDATEDON => date('Y-m-d H:i:s')
            );

            $res = $this->db->where(COL_CATEGORYID, $id)->update(TBL_MCATEGORY, $data);
            if($res) {
                ShowJsonSuccess("Berhasil");
            } else {
                ShowJsonError("Gagal");
            }
        }
    }

    function category_delete(){
        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_MCATEGORY, array(COL_CATEGORYID => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }

    function stock_index() {
        $data['title'] = "Barang";
        $data['res'] = $this->db
            ->join(TBL_MSATUAN,TBL_MSATUAN.'.'.COL_SATUANID." = ".TBL_MSTOCK.".".COL_SATUANID,"left")
            ->join(TBL_MCATEGORY,TBL_MCATEGORY.'.'.COL_CATEGORYID." = ".TBL_MSTOCK.".".COL_CATEGORYID,"left")
            ->get(TBL_MSTOCK)
            ->result_array();
        $this->load->view('master/stock_index', $data);
    }

    function stock_add() {
        $user = GetLoggedUser();
        $data['title'] = "Barang";
        $data['edit'] = FALSE;

        if(!empty($_POST)){
            $data['data'] = $_POST;
            $data = array(
                COL_STOCKNAME => $this->input->post(COL_STOCKNAME),
                COL_STOCKDESCRIPTION => $this->input->post(COL_STOCKDESCRIPTION),
                COL_STOCKBRAND => $this->input->post(COL_STOCKBRAND),
                COL_STOCKICON => $this->input->post(COL_STOCKICON),
                COL_SATUANID => $this->input->post(COL_SATUANID),
                COL_CATEGORYID => $this->input->post(COL_CATEGORYID),
                COL_CREATEDBY => $user[COL_USERNAME],
                COL_CREATEDON => date('Y-m-d H:i:s')
            );
            $res = $this->db->insert(TBL_MSTOCK, $data);
            if($res) {
                redirect('master/stock-index');
            } else {
                redirect(current_url()."?error=1");
            }
        }
        else {
            $this->load->view('master/stock_form', $data);
        }
    }

    function stock_edit($id) {
        $user = GetLoggedUser();
        $data['title'] = "Barang";
        $data['edit'] = TRUE;
        $data['data'] = $edited = $this->db->where(COL_STOCKID, $id)->get(TBL_MSTOCK)->row_array();
        if(empty($edited)){
            show_404();
            return;
        }

        if(!empty($_POST)){
            $data['data'] = $_POST;
            $data = array(
                COL_STOCKNAME => $this->input->post(COL_STOCKNAME),
                COL_STOCKDESCRIPTION => $this->input->post(COL_STOCKDESCRIPTION),
                COL_STOCKBRAND => $this->input->post(COL_STOCKBRAND),
                COL_STOCKICON => $this->input->post(COL_STOCKICON),
                COL_SATUANID => $this->input->post(COL_SATUANID),
                COL_CATEGORYID => $this->input->post(COL_CATEGORYID),
                COL_UPDATEDBY => $user[COL_USERNAME],
                COL_UPDATEDON => date('Y-m-d H:i:s')
            );
            $res = $this->db->where(COL_STOCKID, $id)->update(TBL_MSTOCK, $data);
            if($res) {
                redirect('master/stock-index');
            } else {
                redirect(current_url()."?error=1");
            }
        }
        else {
            $this->load->view('master/stock_form', $data);
        }
    }

    function stock_delete(){
        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_MSTOCK, array(COL_STOCKID => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }

    function fund_index() {
        $data['title'] = "Sumber Dana";
        $data['res'] = $this->db->get(TBL_MFUNDS)->result_array();
        $this->load->view('master/fund_index', $data);
    }

    function fund_add() {
        $user = GetLoggedUser();
        if(!empty($_POST)){
            $data['data'] = $_POST;
            $data = array(
                COL_FUNDNAME => $this->input->post(COL_FUNDNAME),
                COL_CREATEDBY => $user[COL_USERNAME],
                COL_CREATEDON => date('Y-m-d H:i:s')
            );

            $res = $this->db->insert(TBL_MFUNDS, $data);
            if($res) {
                ShowJsonSuccess("Berhasil");
            } else {
                ShowJsonError("Gagal");
            }
        }
    }

    function fund_edit($id) {
        $user = GetLoggedUser();
        if(!empty($_POST)){
            $data['data'] = $_POST;
            $data = array(
                COL_FUNDNAME => $this->input->post(COL_FUNDNAME),
                COL_UPDATEDBY => $user[COL_USERNAME],
                COL_UPDATEDON => date('Y-m-d H:i:s')
            );

            $res = $this->db->where(COL_FUNDID, $id)->update(TBL_MFUNDS, $data);
            if($res) {
                ShowJsonSuccess("Berhasil");
            } else {
                ShowJsonError("Gagal");
            }
        }
    }

    function fund_delete(){
        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_MFUNDS, array(COL_FUNDID => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }

    function satuan_index() {
        $data['title'] = "Satuan";
        $data['res'] = $this->db->get(TBL_MSATUAN)->result_array();
        $this->load->view('master/satuan_index', $data);
    }

    function satuan_add() {
        $user = GetLoggedUser();
        if(!empty($_POST)){
            $data['data'] = $_POST;
            $data = array(
                COL_SATUANNAME => $this->input->post(COL_SATUANNAME),
                COL_CREATEDBY => $user[COL_USERNAME],
                COL_CREATEDON => date('Y-m-d H:i:s')
            );

            $res = $this->db->insert(TBL_MSATUAN, $data);
            if($res) {
                ShowJsonSuccess("Berhasil");
            } else {
                ShowJsonError("Gagal");
            }
        }
    }

    function satuan_edit($id) {
        $user = GetLoggedUser();
        if(!empty($_POST)){
            $data['data'] = $_POST;
            $data = array(
                COL_SATUANNAME => $this->input->post(COL_SATUANNAME),
                COL_UPDATEDBY => $user[COL_USERNAME],
                COL_UPDATEDON => date('Y-m-d H:i:s')
            );

            $res = $this->db->where(COL_SATUANID, $id)->update(TBL_MSATUAN, $data);
            if($res) {
                ShowJsonSuccess("Berhasil");
            } else {
                ShowJsonError("Gagal");
            }
        }
    }

    function satuan_delete(){
        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_MSATUAN, array(COL_SATUANID => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }
}