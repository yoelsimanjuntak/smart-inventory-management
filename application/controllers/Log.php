<?php
/**
 * Created by PhpStorm.
 * User: Toshiba
 * Date: 6/15/2019
 * Time: 5:52 AM
 */
class Log extends MY_Controller {
    function __construct() {
        parent::__construct();
        if(!IsLogin()) {
            redirect('user/login');
        }
    }

    function receipt() {
        $data['title'] = "Barang Masuk";
        $data['res'] = $this->db
            ->join(TBL_MSTOCK,TBL_MSTOCK.'.'.COL_STOCKID." = ".TBL_STOCKPURCHASES.".".COL_STOCKID,"left")
            ->join(TBL_MSATUAN,TBL_MSATUAN.'.'.COL_SATUANID." = ".TBL_MSTOCK.".".COL_SATUANID,"left")
            ->join(TBL_MDEPARTMENT,TBL_MDEPARTMENT.'.'.COL_DEPARTMENTID." = ".TBL_STOCKPURCHASES.".".COL_DEPARTMENTID,"left")
            ->join(TBL_MLOCATION,TBL_MLOCATION.'.'.COL_LOCATIONID." = ".TBL_STOCKPURCHASES.".".COL_LOCATIONID,"left")
            ->join(TBL_MCATEGORY,TBL_MCATEGORY.'.'.COL_CATEGORYID." = ".TBL_MSTOCK.".".COL_CATEGORYID,"left")
            ->get(TBL_STOCKPURCHASES)
            ->result_array();
        $this->load->view('log/receipt', $data);
    }

    function receipt_add() {
        $user = GetLoggedUser();
        $data['title'] = "Barang Masuk";
        $data['edit'] = FALSE;

        if(!empty($_POST)){
          $idStock = $this->input->post(COL_STOCKID);
          $idCategory = 0;

          $rstock = $this->db->where(COL_STOCKID, $idStock)->get(TBL_MSTOCK)->row_array();
          if(empty($rstock)) {

          }
            $data['data'] = $_POST;
            $this->form_validation->set_error_delimiters('<li>', '</li>');
            $this->form_validation->set_rules(array(
                array(
                    'field' => COL_PURCHASEID,
                    'label' => COL_PURCHASEID,
                    'rules' => 'is_unique[stockpurchases.PurchaseID]',
                    'errors' => array('is_unique' => 'No. Penerimaan sudah terdaftar.')
                )/*,
                array(
                    'field' => COL_STOCKID,
                    'label' => COL_STOCKID,
                    'rules' => 'exist[mstock.StockID]',
                    'errors' => array('exist' => 'Barang tidak valid.')
                ),*/
            ));

            if($this->form_validation->run()) {
                $data = array(
                    //COL_PURCHASEID => $this->input->post(COL_PURCHASEID),
                    COL_PURCHASEDATE => date('Y-m-d', strtotime($this->input->post(COL_PURCHASEDATE))),
                    COL_FUNDID => $this->input->post(COL_FUNDID),
                    COL_STOCKID => $this->input->post(COL_STOCKID),
                    //COL_PURCHASENO => $this->input->post(COL_PURCHASENO),
                    COL_DEPARTMENTID => $this->input->post(COL_DEPARTMENTID),
                    COL_LOCATIONID => $this->input->post(COL_LOCATIONID),
                    COL_CONDITION => $this->input->post(COL_CONDITION),
                    COL_PRICE => toNum($this->input->post(COL_PRICE)),
                    COL_PURCHASEQTY => toNum($this->input->post(COL_PURCHASEQTY)),
                    COL_BUDGETYEAR => $this->input->post(COL_BUDGETYEAR),
                    COL_LIFE => toNum($this->input->post(COL_LIFE)),
                    COL_REMARKS => $this->input->post(COL_REMARKS),
                    COL_CREATEDBY => $user[COL_USERNAME],
                    COL_CREATEDON => date('Y-m-d H:i:s')
                );

                $config['upload_path'] = MY_UPLOADPATH;
                $config['allowed_types'] = "gif|jpg|jpeg|png";
                $config['max_size']	= 512000;
                $config['max_width']  = 4000;
                $config['max_height']  = 4000;
                $config['overwrite'] = FALSE;

                $this->load->library('upload',$config);
                $filesCount = count($_FILES['userfile']['name']);
                for($i = 0; $i < $filesCount; $i++) {
                    if(!empty($_FILES['userfile']['name'][$i])) {
                        $_FILES['file']['name'] = $_FILES['userfile']['name'][$i];
                        $_FILES['file']['type'] = $_FILES['userfile']['type'][$i];
                        $_FILES['file']['tmp_name'] = $_FILES['userfile']['tmp_name'][$i];
                        $_FILES['file']['error'] = $_FILES['userfile']['error'][$i];
                        $_FILES['file']['size'] = $_FILES['userfile']['size'][$i];

                        // Upload file to server
                        if($this->upload->do_upload('file')){
                            // Uploaded file data
                            $fileData = $this->upload->data();
                            $uploadData[$i][COL_FILENAME] = $fileData['file_name'];
                        }
                        else {
                            $data['upload_errors'] = $this->upload->display_errors();
                            $this->load->view('log/receipt_form', $data);
                            return;
                        }
                    }
                }
                $this->db->trans_begin();
                $res1 = $this->db->insert(TBL_STOCKPURCHASES, $data);

                $purchaseID = $this->db->insert_id();
                $mvt = array(
                    COL_PURCHASEID => $purchaseID,
                    COL_STOCKMOVEMENTTYPE => MVTTYPE_IN,
                    COL_STOCKID => $data[COL_STOCKID],
                    COL_DEPARTMENTID => $data[COL_DEPARTMENTID],
                    COL_LOCATIONID => $data[COL_LOCATIONID],
                    COL_DATE => $data[COL_PURCHASEDATE],
                    COL_QTY => $data[COL_PURCHASEQTY],
                    COL_CREATEDBY => $user[COL_USERNAME],
                    COL_CREATEDON => date('Y-m-d H:i:s')
                );
                $res2 = $this->db->insert(TBL_STOCKMOVEMENTS, $mvt);

                $res3 = true;
                if(!empty($uploadData)) {
                    for($i = 0; $i < $filesCount; $i++) {
                        $uploadData[$i][COL_PURCHASEID] = $purchaseID;
                    }
                    $res3 = $this->db->insert_batch(TBL_STOCKPURCHASEIMAGES, $uploadData);
                }

                if($res1 && $res2 && $res3) {
                    $this->db->trans_commit();
                    redirect('log/receipt');
                } else {
                    $this->db->trans_rollback();
                    redirect(current_url()."?error=1");
                }
            }
            else {
                $this->load->view('log/receipt_form', $data);
            }
        }
        else {
            $data['data'] = array(COL_BUDGETYEAR => date('Y'));
            $this->load->view('log/receipt_form', $data);
        }
    }

    function receipt_view($id) {
        $user = GetLoggedUser();
        $data['title'] = "Barang Masuk";
        $data['edit'] = TRUE;
        $data['data'] = $edited = $this->db
            ->join(TBL_MSTOCK,TBL_MSTOCK.'.'.COL_STOCKID." = ".TBL_STOCKPURCHASES.".".COL_STOCKID,"left")
            ->join(TBL_MSATUAN,TBL_MSATUAN.'.'.COL_SATUANID." = ".TBL_MSTOCK.".".COL_SATUANID,"left")
            ->join(TBL_MCATEGORY,TBL_MCATEGORY.'.'.COL_CATEGORYID." = ".TBL_MSTOCK.".".COL_CATEGORYID,"left")
            ->where(COL_PURCHASEID, $id)
            ->get(TBL_STOCKPURCHASES)->row_array();
        if(empty($edited)){
            show_404();
            return;
        }

        $this->load->view('log/receipt_form', $data);
    }

    function receipt_delete(){
        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $files = $this->db->where(COL_PURCHASEID, $datum)->get(TBL_STOCKPURCHASEIMAGES)->result_array();
            foreach($files as $f) {
                if(file_exists(MY_UPLOADPATH.$f[COL_FILENAME])) unlink(MY_UPLOADPATH.$f[COL_FILENAME]);
            }
            $this->db->delete(TBL_STOCKPURCHASES, array(COL_PURCHASEID => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }

    function issue() {
        $data['title'] = "Barang Keluar";
        $data['res'] = $this->db
            ->select('*,Origin.'.COL_LOCATIONNAME.' AS OriginName,'.TBL_MLOCATION.'.'.COL_LOCATIONNAME.' AS DestinationName')
            ->join(TBL_STOCKPURCHASES,TBL_STOCKPURCHASES.'.'.COL_PURCHASEID." = ".TBL_STOCKISSUES.".".COL_PURCHASEID,"left")
            ->join(TBL_MSTOCK,TBL_MSTOCK.'.'.COL_STOCKID." = ".TBL_STOCKPURCHASES.".".COL_STOCKID,"left")
            ->join(TBL_MSATUAN,TBL_MSATUAN.'.'.COL_SATUANID." = ".TBL_MSTOCK.".".COL_SATUANID,"left")
            ->join(TBL_MDEPARTMENT,TBL_MDEPARTMENT.'.'.COL_DEPARTMENTID." = ".TBL_STOCKPURCHASES.".".COL_DEPARTMENTID,"left")
            ->join(TBL_MLOCATION.' Origin','Origin.'.COL_LOCATIONID." = ".TBL_STOCKPURCHASES.".".COL_LOCATIONID,"left")
            ->join(TBL_MLOCATION,TBL_MLOCATION.'.'.COL_LOCATIONID." = ".TBL_STOCKISSUES.".".COL_LOCATIONID,"left")
            ->get(TBL_STOCKISSUES)
            ->result_array();
        $this->load->view('log/issue', $data);
    }

    function issue_add() {
        $user = GetLoggedUser();
        $data['title'] = "Barang Keluar";
        $data['edit'] = FALSE;

        if(!empty($_POST)){
            $data['data'] = $_POST;
            $this->form_validation->set_error_delimiters('<li>', '</li>');
            $this->form_validation->set_rules(array(
                array(
                    'field' => COL_PURCHASEID,
                    'label' => COL_PURCHASEID,
                    'rules' => 'required',
                    'errors' => array('is_unique' => 'No. Pengadaan wajib diisi.')
                ),
            ));
            if($this->form_validation->run()) {
                $rec = array(
                    //COL_ISSUEID => $this->input->post(COL_ISSUEID),
                    COL_PURCHASEID => $this->input->post(COL_PURCHASEID),
                    COL_ISSUEID => $this->input->post(COL_ISSUEID),
                    COL_ISSUEDATE => date('Y-m-d', strtotime($this->input->post(COL_ISSUEDATE))),
                    COL_LOCATIONID => $this->input->post(COL_LOCATIONID),
                    COL_ISSUEQTY => toNum($this->input->post(COL_ISSUEQTY)),
                    COL_REMARKS => $this->input->post(COL_REMARKS),
                    COL_CREATEDBY => $user[COL_USERNAME],
                    COL_CREATEDON => date('Y-m-d H:i:s')
                );
                if($rec[COL_ISSUEQTY] <= 0) {
                    $data['errormess'] = 'Jumlah barang tidak valid.';
                    $this->load->view('log/issue_form', $data);
                    return;
                }
                $this->db->trans_begin();

                $receipt = $this->db->where(COL_PURCHASEID, $rec[COL_PURCHASEID])->get(TBL_STOCKPURCHASES)->row_array();
                $receipt_ = $this->db
                    ->select(TBL_STOCKPURCHASES.'.'.COL_PURCHASEID.','.TBL_STOCKPURCHASES.'.'.COL_PURCHASEDATE.', sum(Qty) as QtyLeft')
                    ->join(TBL_STOCKMOVEMENTS,TBL_STOCKMOVEMENTS.'.'.COL_PURCHASEID." = ".TBL_STOCKPURCHASES.".".COL_PURCHASEID.' AND (`stockmovements`.`TransferID` IS NULL OR `stockmovements`.`LocationID` != `stockpurchases`.`LocationID`)',"left")
                    ->where(TBL_STOCKMOVEMENTS.'.'.COL_DEPARTMENTID, $this->input->post(COL_DEPARTMENTID))
                    ->where(TBL_STOCKMOVEMENTS.'.'.COL_LOCATIONID, $this->input->post("OriginID"))
                    ->where(TBL_STOCKMOVEMENTS.'.'.COL_STOCKID, $this->input->post(COL_STOCKID))
                    ->where(TBL_STOCKMOVEMENTS.'.'.COL_PURCHASEID, $this->input->post(COL_PURCHASEID))
                    ->where(TBL_STOCKPURCHASES.'.'.COL_PURCHASEDATE." <=", date('Y-m-d', strtotime($this->input->post(COL_ISSUEDATE))))
                    ->order_by(TBL_STOCKPURCHASES.'.'.COL_PURCHASEDATE, 'desc')
                    ->group_by(TBL_STOCKPURCHASES.'.'.COL_PURCHASEID)
                    ->group_by(TBL_STOCKPURCHASES.'.'.COL_PURCHASEDATE)
                    ->get(TBL_STOCKPURCHASES)->row_array();
                if(empty($receipt)) {
                    $this->db->trans_rollback();
                    $data['errormess'] = 'No. Penerimaan tidak valid.';
                    $this->load->view('log/issue_form', $data);
                    return;
                }

                $config['upload_path'] = MY_UPLOADPATH;
                $config['allowed_types'] = "gif|jpg|jpeg|png";
                $config['max_size']	= 512000;
                $config['max_width']  = 4000;
                $config['max_height']  = 4000;
                $config['overwrite'] = FALSE;

                $this->load->library('upload',$config);
                $filesCount = count($_FILES['userfile']['name']);
                for($i = 0; $i < $filesCount; $i++) {
                    if(!empty($_FILES['userfile']['name'][$i])) {
                        $_FILES['file']['name'] = $_FILES['userfile']['name'][$i];
                        $_FILES['file']['type'] = $_FILES['userfile']['type'][$i];
                        $_FILES['file']['tmp_name'] = $_FILES['userfile']['tmp_name'][$i];
                        $_FILES['file']['error'] = $_FILES['userfile']['error'][$i];
                        $_FILES['file']['size'] = $_FILES['userfile']['size'][$i];

                        // Upload file to server
                        if($this->upload->do_upload('file')){
                            // Uploaded file data
                            $fileData = $this->upload->data();
                            $uploadData[$i][COL_FILENAME] = $fileData['file_name'];
                        }
                        else {
                            $this->db->trans_rollback();
                            $data['upload_errors'] = $this->upload->display_errors();
                            $this->load->view('log/issue_form', $data);
                            return;
                        }
                    }
                }

                $issueID_ = $this->input->post(COL_ISSUEID);
                $itemID_ = $this->input->post(COL_ITEMID);
                if(!empty($issueID_) && !empty($itemID_)) {
                    $q = @"
            SELECT * FROM (
                SELECT
                pur.PurchaseID,
                issue.IssueID,
                i.ItemID,
                (
                    SELECT COALESCE(t_.DestinationID, si.LocationID) AS LastLocationID FROM stockmovements mvt
                    LEFT JOIN stockissues si ON si.IssueID = mvt.IssueID
                    LEFT JOIN stockissueitems i_ ON i_.IssueID = mvt.IssueID
                    LEFT JOIN stocktransfers t_ ON t_.TransferID = mvt.TransferID
                    WHERE
                        mvt.StockID = ?
                        AND i_.ItemID  = i.ItemID
                        AND (t_.TransferID IS NULL OR t_.ItemID = i_.ItemID)
                    ORDER BY
                        mvt.StockMovementType DESC,
                        mvt.Date DESC,
                        mvt.ID DESC
                    LIMIT 1
                ) AS LastLocationID,
                pur.PurchaseNo,
                trf.TransferID
                FROM stockissueitems i
                LEFT JOIN stockissues issue ON issue.IssueID = i.IssueID
                LEFT JOIN stockpurchases pur ON pur.PurchaseID = issue.PurchaseID
                LEFT JOIN stocktransfers trf ON trf.ItemID = i.ItemID
                LEFT JOIN mlocation loc ON loc.LocationID = COALESCE(trf.DestinationID, issue.LocationID)
                WHERE pur.DepartmentID = ? AND pur.StockID = ? and i.ItemID = ? and COALESCE(trf.TransferDate, issue.IssueDate) <= ? AND COALESCE(trf.DestinationID, issue.LocationID) = ?
                ORDER BY COALESCE(issue.IssueDate, trf.TransferDate) DESC
            ) tbl
            LEFT JOIN mlocation loc ON loc.LocationID = tbl.LastLocationID
            GROUP BY PurchaseID, IssueID, ItemID
            ";
                    $stockpos = $this->db->query($q, array(
                            $this->input->post(COL_STOCKID),
                            $this->input->post(COL_DEPARTMENTID),
                            $this->input->post(COL_STOCKID),
                            $this->input->post(COL_ITEMID),
                            date('Y-m-d', strtotime($this->input->post(COL_ISSUEDATE))),
                            $this->input->post("OriginID"))
                    )->row_array();
                    if(empty($stockpos)) {
                        $this->db->trans_rollback();
                        $data['errormess'] = 'No. Item tidak valid / posisi stock pada tanggal mutasi dibawah sudah berubah.';
                        $this->load->view('log/transfer_form', $data);
                        return;
                    }

                    $rec = array(
                        COL_TRANSFERDATE => date('Y-m-d', strtotime($this->input->post(COL_ISSUEDATE))),
                        COL_ITEMID => $this->input->post(COL_ITEMID),
                        COL_DESTINATIONID => $this->input->post(COL_LOCATIONID),
                        COL_PARENTID => $stockpos[COL_TRANSFERID],
                        COL_CONDITION => $this->input->post(COL_CONDITION),
                        COL_REMARKS => $this->input->post(COL_REMARKS),
                        COL_CREATEDBY => $user[COL_USERNAME],
                        COL_CREATEDON => date('Y-m-d H:i:s')
                    );

                    $config['upload_path'] = MY_UPLOADPATH;
                    $config['allowed_types'] = "gif|jpg|jpeg|png";
                    $config['max_size']	= 512000;
                    $config['max_width']  = 4000;
                    $config['max_height']  = 4000;
                    $config['overwrite'] = FALSE;

                    $this->load->library('upload',$config);
                    $filesCount = count($_FILES['userfile']['name']);
                    for($i = 0; $i < $filesCount; $i++) {
                        if(!empty($_FILES['userfile']['name'][$i])) {
                            $_FILES['file']['name'] = $_FILES['userfile']['name'][$i];
                            $_FILES['file']['type'] = $_FILES['userfile']['type'][$i];
                            $_FILES['file']['tmp_name'] = $_FILES['userfile']['tmp_name'][$i];
                            $_FILES['file']['error'] = $_FILES['userfile']['error'][$i];
                            $_FILES['file']['size'] = $_FILES['userfile']['size'][$i];

                            // Upload file to server
                            if($this->upload->do_upload('file')){
                                // Uploaded file data
                                $fileData = $this->upload->data();
                                $uploadData[$i][COL_FILENAME] = $fileData['file_name'];
                            }
                            else {
                                $this->db->trans_rollback();
                                $data['upload_errors'] = $this->upload->display_errors();
                                $this->load->view('log/issue_form', $data);
                                return;
                            }
                        }
                    }
                    $res1 = $this->db->insert(TBL_STOCKTRANSFERS, $rec);

                    $transferID = $this->db->insert_id();
                    $mvt = array(
                        array(
                            COL_PURCHASEID => $stockpos[COL_PURCHASEID],
                            COL_ISSUEID => $stockpos[COL_ISSUEID],
                            COL_TRANSFERID => $transferID,
                            COL_STOCKMOVEMENTTYPE => MVTTYPE_TRF,
                            COL_STOCKID => $this->input->post(COL_STOCKID),
                            COL_DEPARTMENTID => $this->input->post(COL_DEPARTMENTID),
                            COL_LOCATIONID => $stockpos["LastLocationID"],
                            COL_DATE => $rec[COL_TRANSFERDATE],
                            COL_QTY => -1,
                            COL_CREATEDBY => $user[COL_USERNAME],
                            COL_CREATEDON => date('Y-m-d H:i:s')
                        ),
                        array(
                            COL_PURCHASEID => $stockpos[COL_PURCHASEID],
                            COL_ISSUEID => $stockpos[COL_ISSUEID],
                            COL_TRANSFERID => $transferID,
                            COL_STOCKMOVEMENTTYPE => MVTTYPE_TRF,
                            COL_STOCKID => $this->input->post(COL_STOCKID),
                            COL_DEPARTMENTID => $this->input->post(COL_DEPARTMENTID),
                            COL_LOCATIONID => $rec[COL_DESTINATIONID],
                            COL_DATE => $rec[COL_TRANSFERDATE],
                            COL_QTY => 1,
                            COL_CREATEDBY => $user[COL_USERNAME],
                            COL_CREATEDON => date('Y-m-d H:i:s')
                        )
                    );
                    $res2 = $this->db->insert_batch(TBL_STOCKMOVEMENTS, $mvt);

                    $res3 = true;
                    if(!empty($uploadData)) {
                        for($i = 0; $i < $filesCount; $i++) {
                            $uploadData[$i][COL_TRANSFERID] = $transferID;
                        }
                        $res3 = $this->db->insert_batch(TBL_STOCKTRANSFERIMAGES, $uploadData);
                    }

                    if($res1 && $res2 && $res3) {
                        $this->db->trans_commit();
                        redirect('log/transfer');
                    } else {
                        $this->db->trans_rollback();
                        redirect(current_url()."?error=1");
                    }
                } else {
                    if($receipt_["QtyLeft"] < $rec[COL_ISSUEQTY]) {
                        $this->db->trans_rollback();
                        $data['errormess'] = 'Jumlah barang tidak mencukupi.';
                        $this->load->view('log/issue_form', $data);
                        return;
                    }

                    $res1 = $this->db->insert(TBL_STOCKISSUES, $rec);
                    $issueID = $this->db->insert_id();
                    $mvt = array(
                        array(
                            COL_PURCHASEID => $rec[COL_PURCHASEID],
                            COL_ISSUEID => $issueID,
                            COL_STOCKMOVEMENTTYPE => MVTTYPE_OUT,
                            COL_STOCKID => $receipt[COL_STOCKID],
                            COL_DEPARTMENTID => $receipt[COL_DEPARTMENTID],
                            COL_LOCATIONID => $receipt[COL_LOCATIONID],
                            COL_DATE => $rec[COL_ISSUEDATE],
                            COL_QTY => $rec[COL_ISSUEQTY] * -1,
                            COL_CREATEDBY => $user[COL_USERNAME],
                            COL_CREATEDON => date('Y-m-d H:i:s')
                        ),
                        array(
                            COL_PURCHASEID => $rec[COL_PURCHASEID],
                            COL_ISSUEID => $issueID,
                            COL_STOCKMOVEMENTTYPE => MVTTYPE_OUT,
                            COL_STOCKID => $receipt[COL_STOCKID],
                            COL_DEPARTMENTID => $receipt[COL_DEPARTMENTID],
                            COL_LOCATIONID => $rec[COL_LOCATIONID],
                            COL_DATE => $rec[COL_ISSUEDATE],
                            COL_QTY => $rec[COL_ISSUEQTY],
                            COL_CREATEDBY => $user[COL_USERNAME],
                            COL_CREATEDON => date('Y-m-d H:i:s')
                        )
                    );
                    $res2 = $this->db->insert_batch(TBL_STOCKMOVEMENTS, $mvt);

                    $res3 = true;
                    if(!empty($uploadData)) {
                        for($i = 0; $i < $filesCount; $i++) {
                            $uploadData[$i][COL_ISSUEID] = $issueID;
                        }
                        $res3 = $this->db->insert_batch(TBL_STOCKISSUEIMAGES, $uploadData);
                    }

                    $res4 = true;
                    $arrItem = array();
                    if($rec[COL_ISSUEQTY] > 0) {
                        for($i = 0; $i < $rec[COL_ISSUEQTY]; $i++) {
                            $arrItem[] = array(
                                COL_ISSUEID => $issueID
                            );
                        }
                        $res4 = $this->db->insert_batch(TBL_STOCKISSUEITEMS, $arrItem);
                    }

                    if($res1 && $res2 && $res3 && $res4) {
                        $this->db->trans_commit();
                        redirect('log/issue');
                    } else {
                        $this->db->trans_rollback();
                        redirect(current_url()."?error=1");
                    }
                }
            }
            else {
                $this->load->view('log/issue_form', $data);
            }
        }
        else {
            $this->load->view('log/issue_form', $data);
        }
    }

    function issue_view($id) {
        $user = GetLoggedUser();
        $data['title'] = "Barang Keluar";
        $data['edit'] = TRUE;
        $data['data'] = $edited = $this->db
            ->select('*,'.TBL_STOCKPURCHASES.'.'.COL_LOCATIONID.' AS OriginID,'.TBL_STOCKISSUES.'.'.COL_LOCATIONID.' AS LocationID')
            ->join(TBL_STOCKPURCHASES,TBL_STOCKPURCHASES.'.'.COL_PURCHASEID." = ".TBL_STOCKISSUES.".".COL_PURCHASEID,"left")
            ->join(TBL_MSTOCK,TBL_MSTOCK.'.'.COL_STOCKID." = ".TBL_STOCKPURCHASES.".".COL_STOCKID,"left")
            ->join(TBL_MSATUAN,TBL_MSATUAN.'.'.COL_SATUANID." = ".TBL_MSTOCK.".".COL_SATUANID,"left")
            ->where(COL_ISSUEID, $id)->get(TBL_STOCKISSUES)->row_array();
        if(empty($edited)){
            show_404();
            return;
        }

        $this->load->view('log/issue_form', $data);
    }

    function issue_delete(){
        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $files = $this->db->where(COL_ISSUEID, $datum)->get(TBL_STOCKISSUEIMAGES)->result_array();
            foreach($files as $f) {
                if(file_exists(MY_UPLOADPATH.$f[COL_FILENAME])) unlink(MY_UPLOADPATH.$f[COL_FILENAME]);
            }

            $this->db->delete(TBL_STOCKISSUES, array(COL_ISSUEID => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }

    function transfer() {
        $data['title'] = "Mutasi Barang";
        $data['res'] = $this->db
            ->select(TBL_STOCKTRANSFERS.'.*,stockpurchases.PurchaseID,'.TBL_MDEPARTMENT.'.'.COL_DEPARTMENTNAME.', '.TBL_MSTOCK.'.'.COL_STOCKNAME.', '.TBL_STOCKPURCHASES.'.'.COL_PURCHASENO.', '.TBL_MLOCATION.'.'.COL_LOCATIONNAME.', '.TBL_STOCKISSUEITEMS.'.'.COL_ISSUEID.', Origin.'.COL_LOCATIONNAME.' as OriginName')
            ->join(TBL_STOCKISSUEITEMS,TBL_STOCKISSUEITEMS.'.'.COL_ITEMID." = ".TBL_STOCKTRANSFERS.".".COL_ITEMID,"left")
            ->join(TBL_STOCKISSUES,TBL_STOCKISSUES.'.'.COL_ISSUEID." = ".TBL_STOCKISSUEITEMS.".".COL_ISSUEID,"left")
            ->join(TBL_STOCKPURCHASES,TBL_STOCKPURCHASES.'.'.COL_PURCHASEID." = ".TBL_STOCKISSUES.".".COL_PURCHASEID,"left")
            ->join(TBL_MSTOCK,TBL_MSTOCK.'.'.COL_STOCKID." = ".TBL_STOCKPURCHASES.".".COL_STOCKID,"left")
            ->join(TBL_MDEPARTMENT,TBL_MDEPARTMENT.'.'.COL_DEPARTMENTID." = ".TBL_STOCKPURCHASES.".".COL_DEPARTMENTID,"left")
            ->join(TBL_MLOCATION,TBL_MLOCATION.'.'.COL_LOCATIONID." = ".TBL_STOCKTRANSFERS.".".COL_DESTINATIONID,"left")
            ->join(TBL_STOCKTRANSFERS.' Parent','Parent.'.COL_TRANSFERID." = ".TBL_STOCKTRANSFERS.".".COL_PARENTID,"left")
            ->join(TBL_MLOCATION.' Origin','Origin.'.COL_LOCATIONID." = COALESCE(Parent.".COL_DESTINATIONID.",".TBL_STOCKISSUES.'.'.COL_LOCATIONID.")","left")
            ->get(TBL_STOCKTRANSFERS)
            ->result_array();
        $this->load->view('log/transfer', $data);
    }

    function transfer_add() {
        $user = GetLoggedUser();
        $data['title'] = "Mutasi Barang";
        $data['edit'] = FALSE;

        if(!empty($_POST)) {
            $data['data'] = $_POST;

            $q = @"
            SELECT * FROM (
                SELECT
                pur.PurchaseID,
                issue.IssueID,
                i.ItemID,
                (
                    SELECT COALESCE(t_.DestinationID, si.LocationID) AS LastLocationID FROM stockmovements mvt
                    LEFT JOIN stockissues si ON si.IssueID = mvt.IssueID
                    LEFT JOIN stockissueitems i_ ON i_.IssueID = mvt.IssueID
                    LEFT JOIN stocktransfers t_ ON t_.TransferID = mvt.TransferID
                    WHERE
                        mvt.StockID = ?
                        AND i_.ItemID  = i.ItemID
                        AND (t_.TransferID IS NULL OR t_.ItemID = i_.ItemID)
                    ORDER BY
                        mvt.StockMovementType DESC,
                        mvt.Date DESC,
                        mvt.ID DESC
                    LIMIT 1
                ) AS LastLocationID,
                pur.PurchaseNo,
                trf.TransferID
                FROM stockissueitems i
                LEFT JOIN stockissues issue ON issue.IssueID = i.IssueID
                LEFT JOIN stockpurchases pur ON pur.PurchaseID = issue.PurchaseID
                LEFT JOIN stocktransfers trf ON trf.ItemID = i.ItemID
                LEFT JOIN mlocation loc ON loc.LocationID = COALESCE(trf.DestinationID, issue.LocationID)
                WHERE pur.DepartmentID = ? AND pur.StockID = ? and i.ItemID = ? and COALESCE(trf.TransferDate, issue.IssueDate) <= ? AND COALESCE(trf.DestinationID, issue.LocationID) = ?
                ORDER BY COALESCE(issue.IssueDate, trf.TransferDate) DESC
            ) tbl
            LEFT JOIN mlocation loc ON loc.LocationID = tbl.LastLocationID
            GROUP BY PurchaseID, IssueID, ItemID
            ";
            $stockpos = $this->db->query($q, array(
                    $this->input->post(COL_STOCKID),
                    $this->input->post(COL_DEPARTMENTID),
                    $this->input->post(COL_STOCKID),
                    $this->input->post(COL_ITEMID),
                    date('Y-m-d', strtotime($this->input->post(COL_TRANSFERDATE))),
                    $this->input->post("OriginID"))
            )->row_array();
            if(empty($stockpos)) {
                $data['errormess'] = 'No. Item tidak valid / posisi stock pada tanggal mutasi dibawah sudah berubah.';
                $this->load->view('log/transfer_form', $data);
                return;
            }

            $rec = array(
                COL_TRANSFERDATE => date('Y-m-d', strtotime($this->input->post(COL_TRANSFERDATE))),
                COL_ITEMID => $this->input->post(COL_ITEMID),
                COL_DESTINATIONID => $this->input->post(COL_DESTINATIONID),
                COL_PARENTID => $stockpos[COL_TRANSFERID],
                COL_CONDITION => $this->input->post(COL_CONDITION),
                COL_REMARKS => $this->input->post(COL_REMARKS),
                COL_CREATEDBY => $user[COL_USERNAME],
                COL_CREATEDON => date('Y-m-d H:i:s')
            );

            $isreturned = $this->input->post("IsReturned");
            if(!empty($isreturned)) {
                $this->db->select(TBL_STOCKPURCHASES.'.*');
                $this->db->join(TBL_STOCKISSUES,TBL_STOCKISSUES.'.'.COL_ISSUEID." = ".TBL_STOCKISSUEITEMS.".".COL_ISSUEID,"left");
                $this->db->join(TBL_STOCKPURCHASES,TBL_STOCKPURCHASES.'.'.COL_PURCHASEID." = ".TBL_STOCKISSUES.".".COL_PURCHASEID,"left");
                $this->db->where(TBL_STOCKISSUEITEMS.'.'.COL_ITEMID, $rec[COL_ITEMID]);
                $origin = $this->db->get(TBL_STOCKISSUEITEMS)->row_array();
                if(!$origin) {
                    $data['errormess'] = 'No. Item tidak valid / posisi stock pada tanggal mutasi dibawah sudah berubah.';
                    $this->load->view('log/transfer_form', $data);
                    return;
                }

                $rec[COL_DESTINATIONID] = $origin[COL_LOCATIONID];
            }

            $config['upload_path'] = MY_UPLOADPATH;
            $config['allowed_types'] = "gif|jpg|jpeg|png";
            $config['max_size']	= 512000;
            $config['max_width']  = 4000;
            $config['max_height']  = 4000;
            $config['overwrite'] = FALSE;

            $this->load->library('upload',$config);
            $filesCount = count($_FILES['userfile']['name']);
            for($i = 0; $i < $filesCount; $i++) {
                if(!empty($_FILES['userfile']['name'][$i])) {
                    $_FILES['file']['name'] = $_FILES['userfile']['name'][$i];
                    $_FILES['file']['type'] = $_FILES['userfile']['type'][$i];
                    $_FILES['file']['tmp_name'] = $_FILES['userfile']['tmp_name'][$i];
                    $_FILES['file']['error'] = $_FILES['userfile']['error'][$i];
                    $_FILES['file']['size'] = $_FILES['userfile']['size'][$i];

                    // Upload file to server
                    if($this->upload->do_upload('file')){
                        // Uploaded file data
                        $fileData = $this->upload->data();
                        $uploadData[$i][COL_FILENAME] = $fileData['file_name'];
                    }
                    else {
                        $data['upload_errors'] = $this->upload->display_errors();
                        $this->load->view('log/transfer_form', $data);
                        return;
                    }
                }
            }
            $this->db->trans_begin();
            $res1 = $this->db->insert(TBL_STOCKTRANSFERS, $rec);

            $transferID = $this->db->insert_id();
            $mvt = array(
                array(
                    COL_PURCHASEID => $stockpos[COL_PURCHASEID],
                    COL_ISSUEID => $stockpos[COL_ISSUEID],
                    COL_TRANSFERID => $transferID,
                    COL_STOCKMOVEMENTTYPE => MVTTYPE_TRF,
                    COL_STOCKID => $this->input->post(COL_STOCKID),
                    COL_DEPARTMENTID => $this->input->post(COL_DEPARTMENTID),
                    COL_LOCATIONID => $stockpos["LastLocationID"],
                    COL_DATE => $rec[COL_TRANSFERDATE],
                    COL_QTY => -1,
                    COL_CREATEDBY => $user[COL_USERNAME],
                    COL_CREATEDON => date('Y-m-d H:i:s')
                ),
                array(
                    COL_PURCHASEID => $stockpos[COL_PURCHASEID],
                    COL_ISSUEID => $stockpos[COL_ISSUEID],
                    COL_TRANSFERID => $transferID,
                    COL_STOCKMOVEMENTTYPE => MVTTYPE_TRF,
                    COL_STOCKID => $this->input->post(COL_STOCKID),
                    COL_DEPARTMENTID => $this->input->post(COL_DEPARTMENTID),
                    COL_LOCATIONID => $rec[COL_DESTINATIONID],
                    COL_DATE => $rec[COL_TRANSFERDATE],
                    COL_QTY => 1,
                    COL_CREATEDBY => $user[COL_USERNAME],
                    COL_CREATEDON => date('Y-m-d H:i:s')
                )
            );
            $res2 = $this->db->insert_batch(TBL_STOCKMOVEMENTS, $mvt);

            $res3 = true;
            if(!empty($uploadData)) {
                for($i = 0; $i < $filesCount; $i++) {
                    $uploadData[$i][COL_TRANSFERID] = $transferID;
                }
                $res3 = $this->db->insert_batch(TBL_STOCKTRANSFERIMAGES, $uploadData);
            }

            if($res1 && $res2 && $res3) {
                $this->db->trans_commit();
                redirect('log/transfer');
            } else {
                $this->db->trans_rollback();
                redirect(current_url()."?error=1");
            }
        } else {
            $this->load->view('log/transfer_form', $data);
        }
    }

    function transfer_view($id) {
        $user = GetLoggedUser();
        $data['title'] = "Mutasi Barang";
        $data['edit'] = TRUE;
        $data['data'] = $edited = $this->db
            ->select(TBL_STOCKTRANSFERS.'.*,'.TBL_STOCKPURCHASES.'.'.COL_STOCKID.', COALESCE(Parent.'.COL_DESTINATIONID.','.TBL_STOCKISSUES.'.'.COL_LOCATIONID.') as OriginID')
            ->join(TBL_STOCKISSUEITEMS,TBL_STOCKISSUEITEMS.'.'.COL_ITEMID." = ".TBL_STOCKTRANSFERS.".".COL_ITEMID,"left")
            ->join(TBL_STOCKISSUES,TBL_STOCKISSUES.'.'.COL_ISSUEID." = ".TBL_STOCKISSUEITEMS.".".COL_ISSUEID,"left")
            ->join(TBL_STOCKPURCHASES,TBL_STOCKPURCHASES.'.'.COL_PURCHASEID." = ".TBL_STOCKISSUES.".".COL_PURCHASEID,"left")
            ->join(TBL_STOCKTRANSFERS.' Parent','Parent.'.COL_TRANSFERID." = ".TBL_STOCKTRANSFERS.".".COL_PARENTID,"left")
            ->where(TBL_STOCKTRANSFERS.'.'.COL_TRANSFERID, $id)->get(TBL_STOCKTRANSFERS)->row_array();
        if(empty($edited)){
            show_404();
            return;
        }

        $this->load->view('log/transfer_form', $data);
    }

    function transfer_delete(){
        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $files = $this->db->where(COL_TRANSFERID, $datum)->get(TBL_STOCKTRANSFERIMAGES)->result_array();
            foreach($files as $f) {
                if(file_exists(MY_UPLOADPATH.$f[COL_FILENAME])) unlink(MY_UPLOADPATH.$f[COL_FILENAME]);
            }

            $this->db->delete(TBL_STOCKTRANSFERS, array(COL_TRANSFERID => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }
}
