<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 01/10/2018
 * Time: 21:57
 */
class Ajax extends MY_Controller {

    function browse_purchase() {
        $departmentID = $this->input->get("DepartmentID");
        $locationID = $this->input->get("LocationID");
        $stockID = $this->input->get("StockID");

        $this->db->select("stockpurchases.PurchaseID, '' as IssueID, '' as ItemID, stockpurchases.PurchaseDate, stockpurchases.PurchaseNo, stockpurchases.LocationID, msatuan.SatuanName, SUM(stockmovements.Qty) as Qty, CONCAT(LPAD(mstock.CategoryID, 3, '0'),'.',LPAD(stockpurchases.StockID, 3, '0'),'.',LPAD(stockpurchases.PurchaseID, 4, '0'),'.',YEAR(stockpurchases.PurchaseDate)) AS PurchaseNo_");
        $this->db->join(TBL_STOCKMOVEMENTS,TBL_STOCKMOVEMENTS.'.'.COL_PURCHASEID." = ".TBL_STOCKPURCHASES.".".COL_PURCHASEID.' AND (`stockmovements`.`TransferID` IS NULL OR `stockmovements`.`LocationID` != `stockpurchases`.`LocationID`)',"left");
        $this->db->join(TBL_MSTOCK,TBL_MSTOCK.'.'.COL_STOCKID." = ".TBL_STOCKPURCHASES.".".COL_STOCKID,"left");
        $this->db->join(TBL_MSATUAN,TBL_MSATUAN.'.'.COL_SATUANID." = ".TBL_MSTOCK.".".COL_SATUANID,"left");
        $this->db->where(TBL_STOCKMOVEMENTS.'.'.COL_DEPARTMENTID, $departmentID);
        $this->db->where(TBL_STOCKMOVEMENTS.'.'.COL_LOCATIONID, $locationID);
        $this->db->where(TBL_STOCKMOVEMENTS.'.'.COL_STOCKID, $stockID);
        $this->db->order_by(TBL_STOCKPURCHASES.'.'.COL_PURCHASEDATE, 'desc');
        $this->db->group_by(TBL_STOCKPURCHASES.'.'.COL_PURCHASEID);
        $this->db->group_by(TBL_STOCKPURCHASES.'.'.COL_PURCHASEDATE);
        $res1 = $this->db->get(TBL_STOCKPURCHASES)->result_array();
        //echo $this->db->last_query();
        //return;

        $q = @"
        SELECT *, 1 AS Qty FROM (
SELECT
p.PurchaseID,
it.IssueID,
it.ItemID,
p.PurchaseDate,
p.PurchaseNo,
(
	SELECT COALESCE(t_.DestinationID, si.LocationID) AS LastLocationID FROM stockmovements mvt_
	LEFT JOIN stockissueitems it_ ON it_.IssueID = mvt_.IssueID
	LEFT JOIN stocktransfers t_ ON t_.TransferID = mvt_.TransferID
	WHERE
		mvt_.PurchaseID  = p.PurchaseID
		AND mvt_.IssueID = si.IssueID
		AND COALESCE(t_.ItemID, it_.ItemID) = it.ItemID
	ORDER BY
		mvt_.StockMovementType DESC,
		mvt_.Date DESC,
		mvt_.ID DESC
	LIMIT 1
) AS LocationID,
(
	SELECT t_.Condition AS LastConditionID FROM stockmovements mvt_
	LEFT JOIN stockissueitems it_ ON it_.IssueID = mvt_.IssueID
	LEFT JOIN stocktransfers t_ ON t_.TransferID = mvt_.TransferID
	WHERE
		mvt_.PurchaseID  = p.PurchaseID
		AND mvt_.IssueID = si.IssueID
		AND COALESCE(t_.ItemID, it_.ItemID) = it.ItemID
	ORDER BY
		mvt_.StockMovementType DESC,
		mvt_.Date DESC,
		mvt_.ID DESC
	LIMIT 1
) AS ConditionID,
sat.SatuanName,
CONCAT(
    LPAD(s.CategoryID, 3, '0'),'.',
    LPAD(p.StockID, 3, '0'),'.',
    LPAD(p.PurchaseID, 4, '0'),'.',
    YEAR(p.PurchaseDate)
) AS PurchaseNo_
FROM stockpurchases p
LEFT JOIN stockissues si ON si.PurchaseID = p.PurchaseID
LEFT JOIN stockissueitems it ON it.IssueID = si.IssueID
LEFT JOIN mstock s ON s.StockID = p.StockID
LEFT JOIN msatuan sat ON sat.SatuanID = s.SatuanID
WHERE
	p.DepartmentID = ?
	AND p.StockID = ?
) tbl
WHERE
	tbl.LocationID = ?
  AND tbl.ConditionID != ?
        ";
        $res2 = $this->db->query($q, array($departmentID, $stockID, $locationID, CONDTYPE_RUSAK))->result_array();
        $data['res'] = array_merge($res1, $res2);
        $this->load->view('ajax/browse_purchase', $data);
    }

    function browse_item() {
        $departmentID = $this->input->get("DepartmentID");
        $locationID = $this->input->get("LocationID");
        $stockID = $this->input->get("StockID");

        $q = @"
        SELECT * FROM (
            SELECT
            pur.PurchaseID,
            issue.IssueID,
            i.ItemID,
            (
                SELECT COALESCE(t_.DestinationID, si.LocationID) AS LastLocationID FROM stockmovements mvt
                LEFT JOIN stockissues si ON si.IssueID = mvt.IssueID
                LEFT JOIN stockissueitems i_ ON i_.IssueID = mvt.IssueID
                LEFT JOIN stocktransfers t_ ON t_.TransferID = mvt.TransferID AND t_.ItemID = i_.ItemID
                WHERE
                    mvt.StockID = ?
                    AND i_.ItemID = i.ItemID
                    AND (t_.TransferID IS NULL OR t_.ItemID = i_.ItemID)
                ORDER BY
                    mvt.StockMovementType DESC,
                    mvt.Date DESC,
                    mvt.ID DESC
                LIMIT 1
            ) AS LastLocationID,
            pur.PurchaseNo,
            CONCAT(
                LPAD(s.CategoryID, 3, '0'),'.',
                LPAD(pur.StockID, 3, '0'),'.',
                LPAD(pur.PurchaseID, 4, '0'),'.',
                YEAR(pur.PurchaseDate)
            ) AS PurchaseNo_,
            COALESCE(trf.DestinationID, issue.LocationID) AS LocationID
            FROM stockissueitems i
            LEFT JOIN stockissues issue ON issue.IssueID = i.IssueID
            LEFT JOIN stockpurchases pur ON pur.PurchaseID = issue.PurchaseID
            LEFT JOIN mstock s ON s.StockID = pur.StockID
            LEFT JOIN stocktransfers trf ON trf.ItemID = i.ItemID
            LEFT JOIN mlocation loc ON loc.LocationID = COALESCE(trf.DestinationID, issue.LocationID)
            WHERE pur.DepartmentID = ? AND pur.StockID = ?
            ORDER BY COALESCE(issue.IssueDate, trf.TransferDate) DESC
        ) tbl
        LEFT JOIN mlocation loc ON loc.LocationID = tbl.LastLocationID
        WHERE tbl.LastLocationID = ? AND tbl.LocationID = LastLocationID
        GROUP BY PurchaseID, IssueID, ItemID
        ";
        $data['res'] = $this->db->query($q, array($stockID, $departmentID, $stockID, $locationID))->result_array();
        $this->load->view('ajax/browse_item', $data);
    }

    function get_stock_left() {
        $departmentID = $this->input->get("DepartmentID");
        $locationID = $this->input->get("LocationID");
        $stockID = $this->input->get("StockID");
        $purchaseID = $this->input->get("PurchaseID");
        $issueID = $this->input->get("IssueID");
        $itemID = $this->input->get("ItemID");

        $res = array();
        if(empty($issueID) || empty($itemID)) {
            $this->db->select(TBL_STOCKPURCHASES.'.'.COL_PURCHASEID.', sum(Qty) as QtyLeft');
            $this->db->join(TBL_STOCKMOVEMENTS,TBL_STOCKMOVEMENTS.'.'.COL_PURCHASEID." = ".TBL_STOCKPURCHASES.".".COL_PURCHASEID.' AND (`stockmovements`.`TransferID` IS NULL OR `stockmovements`.`LocationID` != `stockpurchases`.`LocationID`)',"left");
            $this->db->where(TBL_STOCKMOVEMENTS.'.'.COL_DEPARTMENTID, $departmentID);
            $this->db->where(TBL_STOCKMOVEMENTS.'.'.COL_LOCATIONID, $locationID);
            $this->db->where(TBL_STOCKMOVEMENTS.'.'.COL_STOCKID, $stockID);
            $this->db->where(TBL_STOCKMOVEMENTS.'.'.COL_PURCHASEID, $purchaseID);
            $this->db->order_by(TBL_STOCKPURCHASES.'.'.COL_PURCHASEDATE, 'desc');
            $this->db->group_by(TBL_STOCKPURCHASES.'.'.COL_PURCHASEID);
            $res = $this->db->get(TBL_STOCKPURCHASES)->row_array();
        } else {
            $q = @"
        SELECT tbl.PurchaseID, 1 AS QtyLeft FROM (
SELECT
p.PurchaseID,
it.IssueID,
it.ItemID,
p.PurchaseDate,
p.PurchaseNo,
(
	SELECT COALESCE(t_.DestinationID, si.LocationID) AS LastLocationID FROM stockmovements mvt_
	LEFT JOIN stockissueitems it_ ON it_.IssueID = mvt_.IssueID
	LEFT JOIN stocktransfers t_ ON t_.TransferID = mvt_.TransferID
	WHERE
		mvt_.PurchaseID  = p.PurchaseID
		AND mvt_.IssueID = si.IssueID
		AND COALESCE(t_.ItemID, it_.ItemID) = it.ItemID
	ORDER BY
		mvt_.StockMovementType DESC,
		mvt_.Date DESC,
		mvt_.ID DESC
	LIMIT 1
) AS LocationID,
sat.SatuanName
FROM stockpurchases p
LEFT JOIN stockissues si ON si.PurchaseID = p.PurchaseID
LEFT JOIN stockissueitems it ON it.IssueID = si.IssueID
LEFT JOIN mstock s ON s.StockID = p.StockID
LEFT JOIN msatuan sat ON sat.SatuanID = s.SatuanID
WHERE
	p.DepartmentID = ?
	AND p.StockID = ?
) tbl
WHERE
	tbl.LocationID = ?
	and tbl.IssueID = ?
	and tbl.ItemID = ?
        ";
            $res = $this->db->query($q, array($departmentID, $stockID, $locationID, $issueID, $itemID))->row_array();
        }

        echo json_encode($res);
        //echo json_encode($res);
    }

    function get_stock_distribution() {
        $departmentID = $this->input->get("DepartmentID");

        /*$q = @"
            SELECT tbl.*, GROUP_CONCAT(DISTINCT cond.ConditionName SEPARATOR ', ') AS LastConditionName, loc.LocationName AS LastLocationName, loc.Latitude, loc.Longitude, GROUP_CONCAT(CONCAT('SIMS.',tbl.PurchaseID,'.',tbl.IssueID,'.',tbl.ItemID) SEPARATOR ', ') AS ItemID, COUNT(*) AS Qty FROM (
                SELECT
                pur.PurchaseID,
                pur.PurchaseNo,
                issue.IssueID,
                pur.StockID,
                stock.StockName,
                stock.StockIcon,
                cat.CategoryName,
                i.ItemID,
                pur.DepartmentID,
                dept.DepartmentName,
                pur.LocationID AS PurchaseLocation,
                locpur.LocationName AS PurchaseLocationName,
                issue.LocationID AS IssueLocation,
                locis.LocationName AS IssueLocationName,
                (
                    SELECT COALESCE(t_.DestinationID, si.LocationID) AS LastLocationID FROM stockmovements mvt
                    LEFT JOIN stockissues si ON si.IssueID = mvt.IssueID
                    LEFT JOIN stockissueitems i_ ON i_.IssueID = mvt.IssueID
                    LEFT JOIN stocktransfers t_ ON t_.TransferID = mvt.TransferID
                    LEFT JOIN mlocation loc_ ON loc_.LocationID = COALESCE(t_.DestinationID, si.LocationID)
                    WHERE
                        mvt.StockID = pur.StockID
                        AND i_.ItemID  = i.ItemID
                        AND COALESCE(t_.ItemID, i_.ItemID) = i_.ItemID
                    ORDER BY
                        mvt.StockMovementType DESC,
                        mvt.Date DESC,
                        mvt.ID DESC
                    LIMIT 1
                ) AS LastLocationID,
                (
                    SELECT COALESCE(t_.Condition, pur_.Condition) AS LastConditionID FROM stockmovements mvt
                    LEFT JOIN stockpurchases pur_ ON pur_.PurchaseID = mvt.PurchaseID
                    LEFT JOIN stockissueitems i_ ON i_.IssueID = mvt.IssueID
                    LEFT JOIN stocktransfers t_ ON t_.TransferID = mvt.TransferID
                    WHERE
                        mvt.StockID = stock.StockID
                        AND i_.ItemID  = i.ItemID
                        AND COALESCE(t_.ItemID, i_.ItemID) = i_.ItemID
                    ORDER BY
                        mvt.StockMovementType DESC,
                        mvt.Date DESC,
                        mvt.ID DESC
                    LIMIT 1
                ) AS LastConditionID
                FROM stockpurchases pur
                LEFT JOIN stockissues issue ON issue.PurchaseID = pur.PurchaseID
                LEFT JOIN stockissueitems i ON i.IssueID = issue.IssueID
                LEFT JOIN mstock stock ON stock.StockID = pur.StockID
                LEFT JOIN mcategory cat ON cat.CategoryID = stock.CategoryID
                LEFT JOIN mdepartment dept ON dept.DepartmentID = pur.DepartmentID
                LEFT JOIN mlocation locpur ON locpur.LocationID = pur.LocationID
                LEFT JOIN mlocation locis ON locis.LocationID = issue.LocationID
            ) tbl
            LEFT JOIN mlocation  loc ON loc.LocationID = tbl.LastLocationID
            LEFT JOIN mcondition cond ON cond.ConditionID = tbl.LastConditionID
            GROUP BY
            tbl.StockID,
            tbl.DepartmentID,
            tbl.LastLocationID
            ";*/
        $q = @"
SELECT
tbl.*,
loc.LocationName AS LastLocationName,
loc.Latitude,
loc.Longitude
FROM (
	SELECT
	(
		SELECT COALESCE(t_.DestinationID, si.LocationID) AS LastLocationID FROM stockmovements mvt
		LEFT JOIN stockissues si ON si.IssueID = mvt.IssueID
		LEFT JOIN stockissueitems i_ ON i_.IssueID = mvt.IssueID
		LEFT JOIN stocktransfers t_ ON t_.TransferID = mvt.TransferID
		WHERE
			mvt.StockID = pur.StockID
			AND i_.ItemID  = i.ItemID
			AND COALESCE(t_.ItemID, i_.ItemID) = i.ItemID
			ORDER BY
			mvt.StockMovementType DESC,
			mvt.Date DESC,
			mvt.ID DESC
		LIMIT 1
		) AS LastLocationID
	FROM stockpurchases pur
	INNER JOIN stockissues issue ON issue.PurchaseID = pur.PurchaseID
	INNER JOIN stockissueitems i ON i.IssueID = issue.IssueID
) tbl
LEFT JOIN mlocation  loc ON loc.LocationID = tbl.LastLocationID
GROUP BY
	tbl.LastLocationID
        ";
        $res = $this->db->query($q)->result_array();
        echo json_encode($res);
    }

    function qrcode($char) {
        $this->load->library('ciqrcode');
        $config['cacheable']	= true; //boolean, the default is true
        $config['cachedir']		= ''; //string, the default is application/cache/
        $config['errorlog']		= ''; //string, the default is application/logs/
        $config['quality']		= true; //boolean, the default is true
        $config['size']			= ''; //interger, the default is 1024
        $config['black']		= array(224,255,255); // array, default is array(255,255,255)
        $config['white']		= array(70,130,180); // array, default is array(0,0,0)

        header("Content-Type: image/png");
        $params['data'] = $char;
        $params['level'] = 'H';
        $params['size'] = 3;
        $this->ciqrcode->generate($params);
    }

	function test() {
		header('Access-Control-Allow-Origin: *');
		$count = $this->input->get('count');
		$arr = array();
		for($i=1; $i<=$count; $i++) {
			$arr[] = array(
				'id'=>$i,
				'username'=>'user'.$i,
				'message'=>'Message '.$i
			);
		}
		echo json_encode($arr);
	}

    function get_stock_satuan() {
        $stockID = $this->input->get(COL_STOCKID);

        $this->db->join(TBL_MSATUAN,TBL_MSATUAN.'.'.COL_SATUANID." = ".TBL_MSTOCK.".".COL_SATUANID,"left");
        $this->db->where(TBL_MSTOCK.'.'.COL_STOCKID, $stockID);
        $res = $this->db->get(TBL_MSTOCK)->row_array();
        echo json_encode($res);
    }

    function get_stock_origin_by_issueid() {
        $itemID = $this->input->get(COL_ITEMID);
        $this->db->select(TBL_STOCKPURCHASES.'.*');
        $this->db->join(TBL_STOCKISSUES,TBL_STOCKISSUES.'.'.COL_ISSUEID." = ".TBL_STOCKISSUEITEMS.".".COL_ISSUEID,"left");
        $this->db->join(TBL_STOCKPURCHASES,TBL_STOCKPURCHASES.'.'.COL_PURCHASEID." = ".TBL_STOCKISSUES.".".COL_PURCHASEID,"left");
        $this->db->where(TBL_STOCKISSUEITEMS.'.'.COL_ITEMID, $itemID);
        $res = $this->db->get(TBL_STOCKISSUEITEMS)->row_array();
        echo json_encode($res);
    }

    function get_stock_by_location() {
        $locationID = $this->input->get(COL_LOCATIONID);
        $rlocation = $this->db->where(COL_LOCATIONID, $locationID)->get(TBL_MLOCATION)->row_array();
        $q = @"
            SELECT tbl.*, GROUP_CONCAT(DISTINCT cond.ConditionName SEPARATOR ', ') AS LastConditionName, loc.LocationName AS LastLocationName, loc.Latitude, loc.Longitude, GROUP_CONCAT(CONCAT('SIMS.',tbl.PurchaseID,'.',tbl.IssueID,'.',tbl.ItemID) SEPARATOR ', ') AS ItemID, COUNT(*) AS Qty FROM (
                SELECT
                pur.PurchaseID,
                pur.PurchaseNo,
                issue.IssueID,
                pur.StockID,
                stock.StockName,
                stock.StockIcon,
                cat.CategoryName,
                i.ItemID,
                pur.DepartmentID,
                dept.DepartmentName,
                pur.LocationID AS PurchaseLocation,
                locpur.LocationName AS PurchaseLocationName,
                issue.LocationID AS IssueLocation,
                locis.LocationName AS IssueLocationName,
                sat.SatuanName,
                (
                    SELECT COALESCE(t_.DestinationID, si.LocationID) AS LastLocationID FROM stockmovements mvt
                    LEFT JOIN stockissues si ON si.IssueID = mvt.IssueID
                    LEFT JOIN stockissueitems i_ ON i_.IssueID = mvt.IssueID
                    LEFT JOIN stocktransfers t_ ON t_.TransferID = mvt.TransferID
                    LEFT JOIN mlocation loc_ ON loc_.LocationID = COALESCE(t_.DestinationID, si.LocationID)
                    WHERE
                        mvt.StockID = pur.StockID
                        AND i_.ItemID  = i.ItemID
                        AND COALESCE(t_.ItemID, i_.ItemID) = i.ItemID
                    ORDER BY
                        mvt.StockMovementType DESC,
                        mvt.Date DESC,
                        mvt.ID DESC
                    LIMIT 1
                ) AS LastLocationID,
                (
                    SELECT COALESCE(t_.Condition, pur_.Condition) AS LastConditionID FROM stockmovements mvt
                    LEFT JOIN stockpurchases pur_ ON pur_.PurchaseID = mvt.PurchaseID
                    LEFT JOIN stockissueitems i_ ON i_.IssueID = mvt.IssueID
                    LEFT JOIN stocktransfers t_ ON t_.TransferID = mvt.TransferID
                    WHERE
                        mvt.StockID = stock.StockID
                        AND i_.ItemID  = i.ItemID
                        AND COALESCE(t_.ItemID, i_.ItemID) = i_.ItemID
                    ORDER BY
                        mvt.StockMovementType DESC,
                        mvt.Date DESC,
                        mvt.ID DESC
                    LIMIT 1
                ) AS LastConditionID
                FROM stockpurchases pur
                LEFT JOIN stockissues issue ON issue.PurchaseID = pur.PurchaseID
                LEFT JOIN stockissueitems i ON i.IssueID = issue.IssueID
                LEFT JOIN mstock stock ON stock.StockID = pur.StockID
                LEFT JOIN msatuan sat ON sat.SatuanID = stock.SatuanID
                LEFT JOIN mcategory cat ON cat.CategoryID = stock.CategoryID
                LEFT JOIN mdepartment dept ON dept.DepartmentID = pur.DepartmentID
                LEFT JOIN mlocation locpur ON locpur.LocationID = pur.LocationID
                LEFT JOIN mlocation locis ON locis.LocationID = issue.LocationID
            ) tbl
            LEFT JOIN mlocation  loc ON loc.LocationID = tbl.LastLocationID
            LEFT JOIN mcondition cond ON cond.ConditionID = tbl.LastConditionID
            WHERE tbl.LastLocationID = ?
            GROUP BY
            tbl.StockID,
            tbl.DepartmentID,
            tbl.LastLocationID
            ";
        $res = $this->db->query($q, array($locationID))->result_array();
        if(empty($res)) {
            echo 'Error';
            return;
        }

        $html = "";
        if(!empty($rlocation)) {
            $html .= '<h4>'.$rlocation[COL_LOCATIONNAME].'</h4><br />';
        }
        $html .= '<table class="table table-bordered">';
        $html .= '<thead><tr><th>No. Pengadaan</th><th>Barang</th><th>Jlh.</th><th>Satuan</th><th>Kondisi</th></tr></thead>';
        $html .= '<tbody>';
        foreach($res as $d) {
            $html .= '<tr>';
            $html .= '<td class="text-right">'.$d[COL_PURCHASENO].'</td>';
            //$html .= '<td class="text-right">'.$d[COL_ITEMID].'</td>';
            $html .= '<td>'.$d[COL_STOCKNAME].'</td>';
            $html .= '<td class="text-right">'.$d["Qty"].'</td>';
            $html .= '<td>'.$d[COL_SATUANNAME].'</td>';
            $html .= '<td>'.$d["LastConditionName"].'</td>';
            $html .= '</tr>';
        }
        $html .= '</tbody>';
        $html .= '</table>';
        echo $html;
    }
}
