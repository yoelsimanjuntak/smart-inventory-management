<?php
/**
 * Created by PhpStorm.
 * User: Toshiba
 * Date: 6/16/2019
 * Time: 11:16 PM
 */
$data = array();
$i = 0;
$n = 1;
foreach ($res as $d) {
    $res[$i] = array(
        $n.'.',
        $d[COL_LOCATIONNAME],
        $d[COL_STOCKNAME],
        !empty($d[COL_SATUANNAME])?$d[COL_SATUANNAME]:'-',
        $d["PurchaseNo_"],
        'SIMS.'.$d[COL_PURCHASEID].'.'.$d[COL_ISSUEID].'.'.$d[COL_ITEMID],
        date('Y-m-d', strtotime($d["LastMovementDate"])),
        date('Y-m-d', strtotime($d[COL_PURCHASEDATE])),
        $d[COL_BUDGETYEAR],
        $d[COL_CONDITIONNAME],
        ''
    );
    $i++;
    $n++;
}
$data = json_encode($res);
$user = GetLoggedUser();
?>

<?php $this->load->view('header')
?>
    <section class="content-header">
        <h1><?= $title ?>  <small>Data</small></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a>
            </li>
            <li class="active">
                Rekapitulasi
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box box-default">
            <?=form_open(current_url(),array('role'=>'form','id'=>'filter-form','class'=>'form-horizontal','method'=>'get'))?>
            <div class="box-body">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label  class="control-label col-sm-4">Instansi</label>
                        <div class="col-sm-8">
                            <select name="<?=COL_DEPARTMENTID?>" class="form-control">
                                <option value="">-- Semua --</option>
                                <?=GetCombobox("SELECT * FROM mdepartment ORDER BY DepartmentName", COL_DEPARTMENTID, COL_DEPARTMENTNAME, (!empty($filter[COL_DEPARTMENTID]) ? $filter[COL_DEPARTMENTID] : null))?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="control-label col-sm-4">Lokasi</label>
                        <div class="col-sm-8">
                            <select name="<?=COL_LOCATIONID?>" class="form-control">
                                <option value="">-- Semua --</option>
                                <?=GetCombobox("SELECT * FROM mlocation ORDER BY LocationName", COL_LOCATIONID, COL_LOCATIONNAME, (!empty($filter[COL_LOCATIONID]) ? $filter[COL_LOCATIONID] : null))?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                      <label  class="control-label col-sm-4">Tanggal</label>
                      <div class="col-sm-3">
                          <input type="text" class="form-control datepicker-ymd" name="<?=COL_DATE?>" value="<?=!empty($filter[COL_DATE]) ? $filter[COL_DATE] : ''?>" required />
                      </div>
                  </div>
                  <div class="form-group">
                      <label  class="control-label col-sm-4">Status</label>
                      <div class="col-sm-6">
                        <select name="<?=COL_CONDITIONID?>" class="form-control">
                            <option value="">-- Semua --</option>
                            <?=GetCombobox("SELECT * FROM mcondition ORDER BY ConditionName", COL_CONDITIONID, COL_CONDITIONNAME, (!empty($filter[COL_CONDITIONID]) ? $filter[COL_CONDITIONID] : null))?>
                        </select>
                      </div>
                  </div>
                </div>
            </div>
            <div class="box-footer" style="text-align: right">
                <button type="button" class="btn btn-default btn-flat btn-sm" id="btn-reset"><i class="fa fa-refresh"></i> RESET</button>
                <button type="submit" class="btn btn-primary btn-flat btn-sm"><i class="fa fa-filter"></i> SUBMIT</button>
            </div>
            <?=form_close()?>
        </div>
        <div class="box box-solid">
            <div class="box-body">
                <form id="dataform" method="post" action="#">
                    <table id="datalist" class="table table-hover nowrap">

                    </table>
                </form>
            </div>
        </div>
    </section>

<?php $this->load->view('loadjs')?>
    <script type="text/javascript">
        $(document).ready(function() {
            var dataTable = $('#datalist').dataTable({
                "aaData": <?=$data?>,
                "ordering": false,
                "paging": false,
                "scrollX": "120%",
                "dom":"R<'row'<'col-sm-4'l><'col-sm-4'B>><'row'<'col-sm-12'tr>>",
                "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
                "order": [[ 1, "desc" ]],
                /*"columnDefs": [
                    { className: "text-right", "targets": [ 4 ] }
                ],*/
                "aoColumns": [
                    {"sTitle": "No.", "sWidth":"10px"},
                    {"sTitle": "Lokasi"},
                    {"sTitle": "Nama Barang"},
                    {"sTitle": "Satuan"},
                    {"sTitle": "No. Pengadaan"},
                    {"sTitle": "No. Item"},
                    {"sTitle": "Tgl. Diterima"},
                    {"sTitle": "Tgl. Pengadaan"},
                    {"sTitle": "T.A"},
                    {"sTitle": "Kondisi"},
                    {"sTitle": "Catatan", "sWidth":"80px"}
                ]
            });
            $("#btn-reset").click(function() {
                $("input", $("#filter-form")).not(".datepicker-ymd").val("");
                $("select", $("#filter-form")).val("").trigger("change");
                $(".datepicker-ymd", $("#filter-form")).val(moment().endOf('month').format('YYYY-MM-DD'));
            });
        });
    </script>

<?php $this->load->view('footer')
?>
