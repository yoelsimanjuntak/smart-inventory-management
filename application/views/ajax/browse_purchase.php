<?php
/**
 * Created by PhpStorm.
 * User: Toshiba
 * Date: 6/15/2019
 * Time: 9:52 AM
 */
$data = array();
$i = 0;
foreach ($res as $d) {
    $res[$i] = array(
        $i+1,
        //$d["Text"]
        date('d/m/Y',strtotime($d[COL_PURCHASEDATE])),
        '<input type="hidden" name="PurchaseID" value="'.$d[COL_PURCHASEID].'" />'.
        '<input type="hidden" name="IssueID" value="'.$d[COL_ISSUEID].'" />'.
        '<input type="hidden" name="ItemID" value="'.$d[COL_ITEMID].'" />'.
        '<input type="hidden" name="Text" value="'.$d[COL_PURCHASENO].'" />'.
        '<input type="hidden" name="StockLeft" value="'.$d[COL_QTY].'" />'.
        $d["PurchaseNo_"]/*.
        (!empty($d[COL_ISSUEID])&&!empty($d[COL_ITEMID])?'<br /><small>SIMS.'.$d[COL_PURCHASEID].'.'.$d[COL_ISSUEID].'.'.$d[COL_ITEMID].'</small>':'')*/,
        number_format($d[COL_QTY]),
        $d[COL_SATUANNAME],
        (!empty($d[COL_ISSUEID])&&!empty($d[COL_ITEMID])?'SIMS.'.$d[COL_PURCHASEID].'.'.$d[COL_ISSUEID].'.'.$d[COL_ITEMID]:'Barang Masuk')
    );
    $i++;
}
$data = json_encode($res);
?>
<style>
    table.table-picker > tbody > tr > td {
        cursor: pointer;
    }
</style>
<form id="dataform" method="post" action="#">
    <input type="hidden" name="selIDPurchase" />
    <input type="hidden" name="selIDIssue" />
    <input type="hidden" name="selIDItem" />
    <input type="hidden" name="selStockLeft" />
    <input type="hidden" name="selText" />
    <table id="browseGrid" class="table table-bordered table-hover nowrap table-picker" width="100%" cellspacing="0">

    </table>
</form>

<script>
    console.log(<?=$data?>);
    $(document).ready(function () {
        var dataTable = $('#browseGrid').dataTable({
            //"sDom": "Rlfrtip",
            "aaData": <?=$data?>,
            //"bJQueryUI": true,
            "aaSorting" : [[1,'desc']],
            //"scrollY" : 400,
            //"scrollX": "200%",
            "iDisplayLength": 10,
            "aLengthMenu": [[10, 100, -1], [10, 100, "Semua"]],
            "dom":"R<'row'<'col-sm-4'l><'col-sm-8'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
            //"buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
            "aoColumns": [
                {"sTitle": "#",bSortable:false,"sWidth":"10px"},
                {"sTitle": "Tanggal Masuk"},
                {"sTitle": "No. Pengadaan"},
                {"sTitle": "Jumlah"},
                {"sTitle": "Satuan"},
                {"sTitle": "Ket."}
            ],
            "createdRow": function (row, data, index) {
                //var elBrowseTable = $("#browseGrid>tbody");
                $(row).dblclick(function () {
                    var elIDPurchase = $(row).find("[name=PurchaseID][type=hidden]").first();
                    var elIDIssue = $(row).find("[name=IssueID][type=hidden]").first();
                    var elIDItem = $(row).find("[name=ItemID][type=hidden]").first();
                    var elText = $(row).find("[name=Text][type=hidden]").first();
                    var elStockLeft = $(row).find("[name=StockLeft][type=hidden]").first();
                    $("[name=selIDIssue][type=hidden]").val(elIDIssue.val()).change();
                    $("[name=selIDItem][type=hidden]").val(elIDItem.val()).change();
                    $("[name=selIDPurchase][type=hidden]").val(elIDPurchase.val()).change();
                    $("[name=selText][type=hidden]").val(elText.val()).change();
                    $("[name=selStockLeft][type=hidden]").val(elStockLeft.val()).change();
                    $(row).closest(".modal").find("button[data-dismiss=modal]").click();
                    $(row).closest(".modal-body").empty();
                });
            }
        });
    });
</script>
<div class="clearfix"></div>
