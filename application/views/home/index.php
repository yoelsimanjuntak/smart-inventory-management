<?php $this->load->view('header-front') ?>
    <style>
        /* Set the size of the div element that contains the map */
        #map {
            height: 64vh;  /* The height is 400 pixels */
            width: 100%;  /* The width is the width of the web page */
        }
        #map td {
            padding: 5px;
        }
    </style>

<div class="row">
    <div class="col-sm-8">
        <div class="box box-solid">
            <div class="box-body">
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="item active">
                            <img src="<?=MY_IMAGEURL.'slider/slider-1.jpg'?>" style="height: 300px; width: 100%">
                        </div>
                        <div class="item">
                            <img src="<?=MY_IMAGEURL.'slider/slider-2.jpg'?>" style="height: 300px; width: 100%">
                        </div>
                        <div class="item">
                            <img src="<?=MY_IMAGEURL.'slider/slider-3.jpg'?>" style="height: 300px; width: 100%">
                        </div>
                    </div>
                    <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                        <span class="fa fa-angle-left"></span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                        <span class="fa fa-angle-right"></span>
                    </a>
                </div>
            </div>
            <!-- /.box-body -->
        </div>

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#location" data-toggle="tab">Lokasi Barang Keluar</a></li>
                <li><a href="#news" data-toggle="tab">Berita Terbaru</a></li>
                <li><a href="#gallery" data-toggle="tab">Galeri</a></li>
            </ul>
            <div class="tab-content">
                <div class="active tab-pane" id="location">
                    <p style="font-style: italic"><span class="text-red">*)</span> Klik pada marker peta untuk melihat informasi detail</p>
                    <div id="map"></div>
                </div>
                <div class="tab-pane" id="news">
                    <?php
                    if(!empty($news) && count($news) > 0) {
                        foreach($news as $n) {
                            ?>
                            <!-- Post -->
                            <div class="post clearfix">
                                <div class="user-block">
                                    <img class="img-circle img-bordered-sm" src="<?=MY_IMAGEURL.'logo.png'?>" alt="Logo">
                                    <span class="username">
                                        <a href="<?=site_url('post/view/'.$n[COL_POSTSLUG])?>"><?=$n[COL_POSTTITLE]?></a>
                                    </span>
                                    <span class="description"><i class="fa fa-user"></i>&nbsp;&nbsp;<?=$n[COL_NAME]?> - <?=date('d M Y H:i')?></span>
                                </div>
                                <!-- /.user-block -->
                                <?php
                                $strippedcontent = strip_tags($n[COL_POSTCONTENT]);
                                ?>
                                <p style="text-align: justify">
                                    <?=strlen($strippedcontent) > 400 ? substr($strippedcontent, 0, 400) . "..." : $strippedcontent ?>
                                </p>
                                <ul class="list-inline">
                                    <li class="pull-right">
                                        <span class="text-sm">
                                            <i class="fa fa-eye"></i> Dilihat <b><?=number_format($n[COL_TOTALVIEW], 0)?></b> kali
                                        </span>
                                    </li>
                                </ul>
                            </div>
                            <!-- /.post -->
                            <?php
                        }
                    } else {
                        ?>
                        <blockquote>
                            <p>Maaf, tidak ada data untuk ditampilkan</p>
                        </blockquote>
                        <?php
                    }
                    ?>

                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="gallery">
                    <?php
                    if(!empty($gallery) && count($gallery) > 0) {
                        foreach($gallery as $n) {
                            $files = $this->db->where(COL_POSTID, $n[COL_POSTID])->get(TBL_POSTIMAGES)->result_array();
                            ?>
                            <div class="post clearfix">
                                <div class="user-block">
                                    <img class="img-circle img-bordered-sm" src="<?=MY_IMAGEURL.'logo.png'?>" alt="Logo">
                                    <span class="username">
                                        <a href="<?=site_url('post/view/'.$n[COL_POSTSLUG])?>"><?=$n[COL_POSTTITLE]?></a>
                                    </span>
                                    <span class="description"><i class="fa fa-user"></i>&nbsp;&nbsp;<?=$n[COL_NAME]?> - <?=date('d M Y H:i')?></span>
                                </div>
                                <!-- /.user-block -->
                                <div class="row margin-bottom">
                                    <div class="col-sm-12" style="display: flex; flex-flow: column wrap; text-align: center">
                                        <div class="lightBoxGallery" style="text-align: left; width: 100%">
                                        <?php
                                        if(count($files) > 1) {
                                            foreach($files as $f) {
                                                ?>
                                                <a href="<?=MY_UPLOADURL.$f[COL_FILENAME]?>" title="<?=$n[COL_POSTTITLE]?>" data-gallery="">
                                                    <img src="<?=MY_UPLOADURL.$f[COL_FILENAME]?>" alt="<?=$n[COL_POSTTITLE]?>" style="max-width: 50vh; margin: 5px; box-shadow: 0 1px 1px rgba(0,0,0,0.2)" />
                                                </a>
                                                <!--<img src="<?=MY_UPLOADURL.$f[COL_FILENAME]?>" alt="<?=$n[COL_POSTTITLE]?>" style="border: 1.5px solid #dedede; max-width: 100%">-->
                                            <?php
                                            }
                                        }
                                        else if(count($files) > 0) {
                                            ?>
                                            <a href="<?=MY_UPLOADURL.$files[0][COL_FILENAME]?>" title="<?=$n[COL_POSTTITLE]?>" data-gallery="">
                                                <img src="<?=MY_UPLOADURL.$files[0][COL_FILENAME]?>" alt="<?=$n[COL_POSTTITLE]?>" style="width: 100%; padding: 5px" />
                                            </a>
                                            <?php
                                        }
                                        ?>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.row -->

                                <ul class="list-inline">
                                    <li class="pull-right">
                                        <span class="text-sm">
                                            <i class="fa fa-eye"></i> Dilihat <b><?=number_format($n[COL_TOTALVIEW], 0)?></b> kali
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        <?php
                        }
                    } else {
                        ?>
                        <blockquote>
                            <p>Maaf, tidak ada data untuk ditampilkan</p>
                        </blockquote>
                    <?php
                    }
                    ?>
                </div>
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>
    </div>
    <?php $this->load->view('sidebar-front') ?>
</div>
    <div id="blueimp-gallery" class="blueimp-gallery">
        <div class="slides"></div>
        <h3 class="title"></h3>
        <a class="prev">‹</a>
        <a class="next">›</a>
        <a class="close">×</a>
        <a class="play-pause"></a>
        <ol class="indicator"></ol>
    </div>

<?php $this->load->view('footer-front') ?>
<script>
    var map; //Will contain map object.
    var marker = false; ////Has the user plotted their location marker?
    var gis;

    //Function called to initialize / create the map.
    //This is called when the page has loaded.
    function initMap() {
        //The center location of our map.
        var centerOfMap = new google.maps.LatLng(2.2587974,98.7436999);
        var infowindow = new google.maps.InfoWindow();

        //Map options.
        var options = {
            center: centerOfMap,
            zoom: 13
        };

        //Create the map object.
        map = new google.maps.Map(document.getElementById('map'), options);
        /*var currLocation = new google.maps.Marker({
         position: centerOfMap,
         map: map,
         title: 'Lokasi sekarang',
         icon: {
         url: "http://maps.google.com/mapfiles/ms/icons/red-dot.png"
         }
         });*/
        $.get("<?=site_url("ajax/get-stock-distribution")?>", function(dat) {
            var data = JSON.parse(dat);
            if(data) {
                for(var i = 0; i < data.length; i++){
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(parseFloat(data[i].Latitude), data[i].Longitude),
                        map: map,
                        /*icon: {
                            url: '<?=MY_IMAGEURL."marker/"?>'+data[i].StockIcon
                        },*/
                    });
                    gis = data[i].LastLocationID;

                    /*gis = '<h4>'+data[i].LastLocationName+'</h4>';
                    gis += '<table>';
                    gis += '<tr><td>Instansi</td><td>:</td><td><b>'+data[i].DepartmentName+'</b></td></tr>';
                    gis += '<tr><td>Kategori</td><td>:</td><td><b>'+data[i].CategoryName+'</b></td></tr>';
                    gis += '<tr><td>Barang</td><td>:</td><td><b>'+data[i].StockName+'</b></td></tr>';
                    gis += '<tr><td>No. Tracking</td><td>:</td><td><b>'+data[i].PurchaseNo+'</b></td></tr>';
                    gis += '<tr><td>No. Item</td><td>:</td><td><b>'+data[i].ItemID+'</b></td></tr>';
                    gis += '<tr><td>Jlh</td><td>:</td><td><b>'+desimal(data[i].Qty,0)+'</b></td></tr>';
                    gis += '<tr><td>Kondisi</td><td>:</td><td><b>'+data[i].LastConditionName+'</b></td></tr>';
                    gis += '</table>';*/


                    google.maps.event.addListener(marker, 'click', (function(marker,gis) {
                        return function(){
                            $.get("<?=site_url("ajax/get-stock-by-location")?>", {LocationID: gis}, function(dat) {
                                infowindow.setContent(dat);
                                infowindow.open(map, marker);
                            });
                        }
                    })
                    (marker,gis));
                }
            }
            else {

            }
        }).fail(function() {
            console.log('Gagal menampilkan penanda lokasi.');
        });
    }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDzvhEg4efwXcYvdMvRLDGUEau4vjh8klg&callback=initMap">
</script>