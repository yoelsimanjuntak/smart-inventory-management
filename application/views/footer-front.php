</section>
</div>
<!-- /.container -->
</div>
<div class="jumbotron jumbotron-footer">
<div class="container">
    <div class="row">
        <div class="col-sm-3">
            <strong>Dinas Komunikasi Dan Informatika Kabupaten Humbang Hasundutan</strong>
            <div>Jl. SM. Raja Kompleks Perkantoran Tano Tubu, Doloksanggul 22457</div>
            <table border="0" cellpadding="1" cellspacing="1" style="width:100%">
                <tbody>
                <tr>
                    <td valign="top">Telp</td>
                    <td>:</td>
                    <td>&nbsp;(0633) 31555</td>
                </tr>
                <tr>
                    <td>Email&nbsp;</td>
                    <td>:</td>
                    <td colspan="4">&nbsp;diskominfo@humbanghasundutankab.go.id<br></td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-sm-3">
            <ul style="  list-style: none;" id="link-odd">
                <li><strong>Website lainnya</strong></li>
            </ul>
        </div>
        <div class="col-sm-3">
            <ul style="  list-style: none;" id="link-even">
                <li>&nbsp;</li>
            </ul>
        </div>
        <script type="text/javascript">
            $(document).ready(function(){
                $.get("https://diskominfo.humbanghasundutankab.go.id/index.php/api/data_footer_link",function(x){
                    i=0;
                    $.each(x,function(y,z){
                        if(i%2==0)
                        {
                            $("#link-odd").append('<li><i class="fa fa-globe" style="margin-right:10px;"></i>'+
                            '<a href="'+z.link+'" target="_blank"> '+z.judul+'</a>'+
                            '</li>');

                        }else{
                            $("#link-even").append('<li><i class="fa fa-globe" style="margin-right:10px;"></i>'+
                            '<a href="'+z.link+'" target="_blank"> '+z.judul+'</a>'+
                            '</li>');

                        }

                        i++;
                    })
                })
            })
        </script>
        <div class="col-sm-3">
            <h6><b>Statistik Pengunjung</b></h6>
            <div id="limb_counter">Loading Counter...</div>
            <meta content="text/html;charset=utf-8" http-equiv="Content-Type"><meta content="utf-8" http-equiv="encoding">
            <script>
                var limb_width_counter ='100%';
                var limb_height_counter ='130px';
                var limb_id_counter = 'limb_counter';
            </script>
            </script>
            <script src="//codec.humbanghasundutankab.go.id/counter/counter.js"></script>
        </div>

    </div>
</div>
</div>
<!-- /.content-wrapper -->
<footer class="main-footer footer-front">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; <?=date("Y")?> Smart Inventory Management System</strong>. Strongly developed by <b>Partopi Tao</b>.
</footer>
</div>
<!-- ./wrapper -->

<!-- Bootstrap 3.3.6 -->
<script src="<?=base_url()?>assets/themes/adminlte/bootstrap/js/bootstrap.js"></script>
<!-- SlimScroll -->
<script src="<?=base_url()?>assets/themes/adminlte/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?=base_url()?>assets/themes/adminlte/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?=base_url()?>assets/themes/adminlte/dist/js/app.min.js"></script>

<script src="<?=base_url()?>assets/themes/inspinia/js/plugins/blueimp/jquery.blueimp-gallery.min.js"></script>

<script type="text/javascript" src="<?=base_url()?>assets/template/js/function.js"></script>

<script>
    $('a[href="<?=current_url()?>"]').addClass('active').parents('li').addClass('active');
</script>
</body>
</html>