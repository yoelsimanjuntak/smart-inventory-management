<?php
/**
 * Created by PhpStorm.
 * User: Toshiba
 * Date: 6/18/2019
 * Time: 10:46 PM
 */
$this->load->view('header') ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?= $title ?> <small> Form</small></h1>
        <ol class="breadcrumb">
            <li><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?=site_url('log/transfer')?>"> Mutasi Barang</a></li>
            <li class="active"><?=$edit?'Edit':'Add'?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="box box-primary" style="border-top-color: transparent">
                    <div class="box-body">
                        <?php if(validation_errors()){ ?>
                            <div class="alert alert-danger">
                                <i class="fa fa-ban"></i> PESAN ERROR :
                                <ul>
                                    <?= validation_errors() ?>
                                </ul>

                            </div>
                        <?php } ?>

                        <?php if(!empty($errormess)){ ?>
                            <div class="alert alert-danger">
                                <i class="fa fa-ban"></i> PESAN ERROR :
                                <?= $errormess ?>
                            </div>
                        <?php } ?>

                        <?php  if($this->input->get('success')){ ?>
                            <div class="form-group alert alert-success alert-dismissible">
                                <i class="fa fa-check"></i>
                                Berhasil.
                            </div>
                        <?php } ?>

                        <?php  if($this->input->get('error')){ ?>
                            <div class="form-group alert alert-danger alert-dismissible">
                                <i class="fa fa-ban"></i>
                                Gagal mengupdate data, silahkan coba kembali
                            </div>
                        <?php } ?>

                        <?=form_open_multipart(current_url(),array('role'=>'form','id'=>'stock-form','class'=>'form-horizontal'))?>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Instansi</label>
                                <div class="col-sm-8">
                                    <select name="<?=COL_DEPARTMENTID?>" class="form-control" required>
                                        <?=GetCombobox("SELECT * FROM mdepartment ORDER BY DepartmentName", COL_DEPARTMENTID, COL_DEPARTMENTNAME, (!empty($data[COL_DEPARTMENTID]) ? $data[COL_DEPARTMENTID] : null))?>
                                    </select>
                                </div>
                            </div>
                            <!--<div class="form-group">
                                <label  class="control-label col-sm-4">No. Pengeluaran</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="<?=COL_ISSUEID?>" value="<?=!empty($data[COL_ISSUEID]) ? $data[COL_ISSUEID] : ''?>" required />
                                </div>
                            </div>-->
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Lokasi</label>
                                <div class="col-sm-8">
                                    <select name="OriginID" class="form-control" required>
                                        <option value="">-- Pilih --</option>
                                        <?=GetCombobox("SELECT * FROM mlocation ORDER BY LocationName", COL_LOCATIONID, COL_LOCATIONNAME, (!empty($data["OriginID"]) ? $data["OriginID"] : null))?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Nama Barang</label>
                                <div class="col-sm-8">
                                    <select name="<?=COL_STOCKID?>" class="form-control" required>
                                        <option value="">-- Pilih --</option>
                                        <?=GetCombobox("SELECT * FROM mstock ORDER BY StockName", COL_STOCKID, COL_STOCKNAME, (!empty($data[COL_STOCKID]) ? $data[COL_STOCKID] : null))?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">No. Item</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="<?=COL_ITEMID?>" value="<?=!empty($data[COL_ITEMID]) ? $data[COL_ITEMID] : ''?>" readonly required />
                                        <div class="input-group-btn">
                                            <button type="button" class="btn btn-default btn-flat btn-browse-bid" data-toggle="modal" data-target="#browseItem" data-toggle="tooltip" data-placement="top" title="Browse"><i class="fa fa-ellipsis-h"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Tanggal</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control datepicker" name="<?=COL_TRANSFERDATE?>" value="<?=!empty($data[COL_TRANSFERDATE]) ? $data[COL_TRANSFERDATE] : ''?>" required />
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <?php
                            if(!$edit) {
                                ?>
                                <div class="form-group">
                                    <div class="col-sm-offset-4 col-sm-8">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="IsReturned"> Kembalikan ke Asal
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Tujuan</label>
                                <div class="col-sm-8">
                                    <select name="<?=COL_DESTINATIONID?>" class="form-control" required>
                                        <option value="">-- Pilih --</option>
                                        <?=GetCombobox("SELECT * FROM mlocation ORDER BY LocationName", COL_LOCATIONID, COL_LOCATIONNAME, (!empty($data[COL_DESTINATIONID]) ? $data[COL_DESTINATIONID] : null))?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Kondisi</label>
                                <div class="col-sm-8">
                                    <select name="<?=COL_CONDITION?>" class="form-control" required>
                                        <?=GetCombobox("SELECT * FROM mcondition ORDER BY ConditionName", COL_CONDITIONID, COL_CONDITIONNAME, (!empty($data[COL_CONDITION]) ? $data[COL_CONDITION] : null))?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Catatan</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" rows="4" name="<?=COL_REMARKS?>"><?=!empty($data[COL_REMARKS]) ? $data[COL_REMARKS] : ''?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4">Gambar</label>
                                <div class="col-sm-8">
                                    <input type="file" name="userfile[]" accept="image/*" multiple />
                                    <p class="help-block">Opsional, bisa lebih dari 1 file, Maks. 50MB</p>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <?php
                                if(!empty($data[COL_TRANSFERID])) {
                                    $files_ = $this->db->where(COL_TRANSFERID, $data[COL_TRANSFERID])->get(TBL_STOCKTRANSFERIMAGES)->result_array();
                                    if(count($files_) > 0) {
                                        ?>
                                        <div class="box box-info box-solid">
                                            <div class="box-header with-border">
                                                <h3 class="box-title">GAMBAR / FOTO</h3>

                                                <div class="box-tools pull-right">
                                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="box-body">
                                                <?php
                                                foreach($files_ as $f) {
                                                    ?>
                                                    <a href="<?=MY_UPLOADURL.$f[COL_FILENAME]?>" target="_blank">
                                                        <img data-toggle="modal" src="<?=MY_UPLOADURL.$f[COL_FILENAME]?>" style="max-height: 20vh; border: 1.5px solid #dedede; box-shadow: 3px 4px 5px rgba(0,0,0,0.2); margin: 10px" />
                                                    </a>
                                                <?php
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    <?php
                                    }
                                } ?>
                            </div>
                        </div>
                        <hr />
                        <div class="col-sm-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-flat pull-right">Simpan</button>
                            </div>
                        </div>
                        <?=form_close()?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" id="browseItem" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

<?php $this->load->view('loadjs') ?>
    <script>
        $(document).ready(function() {
            <?php
            if($edit) {
            ?>
            $('input, button, select, textarea').not('.btn-box-tool').attr('disabled', true);
            <?php
            }
            ?>

            $('#browseItem').on('show.bs.modal', function (event) {
                var modalBody = $(".modal-body", $("#browseItem"));
                $(this).removeData('bs.modal');

                var departmentID = $("[name=DepartmentID]").val();
                var locationID = $("[name=OriginID]").val();
                var stockID = $("[name=StockID]").val();

                if(!departmentID || !locationID || !stockID) {
                    modalBody.html("<p style='font-style: italic'>Silakan pilih instansi, lokasi dan barang terlebih dahulu!</p>");
                    return;
                }

                modalBody.html("<p style='font-style: italic'>Loading..</p>");
                modalBody.load("<?=site_url("ajax/browse-item")?>"+"?DepartmentID="+departmentID+"&LocationID="+locationID+"&StockID="+stockID, function () {
                    $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                        $("[name=ItemID]").val($(this).val()).change();
                    });
                });
            });

            $("[name=OriginID], [name=DepartmentID], [name=StockID]").change(function() {
                var itemID = $("[name=ItemID]").val();
                if(itemID != '') {
                    $("[name=ItemID]").val('');
                }
            });

            $("[name=IsReturned]").click(function(e) {
                if($(this).is(':checked')) {
                    var issueNo = $('[name=ItemID]').val();
                    if(!issueNo) {
                        alert('Silakan pilih item terlebih dahulu!');
                        e.preventDefault();
                        return false;
                    }

                    $.get("<?=site_url("ajax/get-stock-origin-by-issueid")?>", {ItemID: issueNo}, function(res) {
                        var dat = JSON.parse(res);
                        if(dat && dat.LocationID) {
                            $('[name=DestinationID]').val(dat.LocationID).change();
                            $('[name=DestinationID]').attr('disabled', true);
                        } else {

                        }
                    }).fail(function() {
                        console.log('Origin error.');
                    });
                } else {
                    $('[name=DestinationID]').attr('disabled', false);
                }
            });
        });
    </script>
<?php $this->load->view('footer') ?>