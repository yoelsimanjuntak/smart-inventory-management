<?php
/**
 * Created by PhpStorm.
 * User: Toshiba
 * Date: 6/15/2019
 * Time: 8:24 AM
 */
$this->load->view('header') ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?= $title ?> <small> Form</small></h1>
        <ol class="breadcrumb">
            <li><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?=site_url('log/issue')?>"> Barang Keluar</a></li>
            <li class="active"><?=$edit?'Edit':'Add'?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="box box-primary" style="border-top-color: transparent">
                    <div class="box-body">
                        <?php if(validation_errors()){ ?>
                            <div class="alert alert-danger">
                                <i class="fa fa-ban"></i> PESAN ERROR :
                                <ul>
                                    <?= validation_errors() ?>
                                </ul>

                            </div>
                        <?php } ?>

                        <?php if(!empty($errormess)){ ?>
                            <div class="alert alert-danger">
                                <i class="fa fa-ban"></i> PESAN ERROR :
                                <?= $errormess ?>
                            </div>
                        <?php } ?>

                        <?php  if($this->input->get('success')){ ?>
                            <div class="form-group alert alert-success alert-dismissible">
                                <i class="fa fa-check"></i>
                                Berhasil.
                            </div>
                        <?php } ?>

                        <?php  if($this->input->get('error')){ ?>
                            <div class="form-group alert alert-danger alert-dismissible">
                                <i class="fa fa-ban"></i>
                                Gagal mengupdate data, silahkan coba kembali
                            </div>
                        <?php } ?>

                        <?=form_open_multipart(current_url(),array('role'=>'form','id'=>'stock-form','class'=>'form-horizontal'))?>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Instansi</label>
                                <div class="col-sm-8">
                                    <select name="<?=COL_DEPARTMENTID?>" class="form-control" required>
                                        <?=GetCombobox("SELECT * FROM mdepartment ORDER BY DepartmentName", COL_DEPARTMENTID, COL_DEPARTMENTNAME, (!empty($data[COL_DEPARTMENTID]) ? $data[COL_DEPARTMENTID] : null))?>
                                    </select>
                                </div>
                            </div>
                            <!--<div class="form-group">
                                <label  class="control-label col-sm-4">No. Pengeluaran</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="<?=COL_ISSUEID?>" value="<?=!empty($data[COL_ISSUEID]) ? $data[COL_ISSUEID] : ''?>" required />
                                </div>
                            </div>-->
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Lokasi</label>
                                <div class="col-sm-8">
                                    <select name="OriginID" class="form-control" required>
                                        <option value="">-- Pilih --</option>
                                        <?=GetCombobox("SELECT * FROM mlocation ORDER BY LocationName", COL_LOCATIONID, COL_LOCATIONNAME, (!empty($data["OriginID"]) ? $data["OriginID"] : null))?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Nama Barang</label>
                                <div class="col-sm-8">
                                    <select name="<?=COL_STOCKID?>" class="form-control" required>
                                        <option value="">-- Pilih --</option>
                                        <?=GetCombobox("SELECT * FROM mstock ORDER BY StockName", COL_STOCKID, COL_STOCKNAME, (!empty($data[COL_STOCKID]) ? $data[COL_STOCKID] : null))?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">No. Pengadaan</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="<?=COL_PURCHASENO?>" value="<?=!empty($data[COL_PURCHASENO]) ? $data[COL_PURCHASENO] : ''?>" readonly required />
                                        <input type="hidden" name="<?=COL_PURCHASEID?>" value="<?=!empty($data[COL_PURCHASEID]) ? $data[COL_PURCHASEID] : ''?>" required />
                                        <div class="input-group-btn">
                                            <button type="button" class="btn btn-default btn-flat btn-browse-bid" data-toggle="modal" data-target="#browsePurchase" data-toggle="tooltip" data-placement="top" title="Browse"><i class="fa fa-ellipsis-h"></i></button>
                                        </div>
                                    </div>
                                    <p class="help-block">Sisa barang : <b id="label-stock-left">0</b>&nbsp;<span class="satuan"><?=!empty($data[COL_SATUANNAME]) ? $data[COL_SATUANNAME] : '--'?></span></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Tanggal</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control datepicker" name="<?=COL_ISSUEDATE?>" value="<?=!empty($data[COL_ISSUEDATE]) ? $data[COL_ISSUEDATE] : ''?>" required />
                                </div>
                            </div>
                            <div class="form-group" <?=!empty($data[COL_ISSUEID])&&!empty($data[COL_ITEMID])?'':'style="display:none"'?>>
                                <label  class="control-label col-sm-4">No. Item</label>
                                <div class="col-sm-4">
                                    <div class="input-group">
                                        <span class="input-group-addon">SIMS.</span>
                                        <input type="text" class="form-control" name="ItemNo" value="<?=!empty($data[COL_ISSUEID])&&!empty($data[COL_ITEMID]) ? $data[COL_PURCHASEID].".".$data[COL_ISSUEID].".".$data[COL_ITEMID] : ''?>" readonly />
                                        <input type="hidden" name="<?=COL_ISSUEID?>" value="<?=!empty($data[COL_ISSUEID]) ? $data[COL_ISSUEID] : ''?>" />
                                        <input type="hidden" name="<?=COL_ITEMID?>" value="<?=!empty($data[COL_ITEMID]) ? $data[COL_ITEMID] : ''?>" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Jumlah</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control uang text-right" name="<?=COL_ISSUEQTY?>" value="<?=!empty($data[COL_ISSUEQTY]) ? $data[COL_ISSUEQTY] : ''?>" required />
                                </div>
                                <div class="col-sm-4">
                                    <span class="satuan control-label" style="float: left;"><span class="satuan"><?=!empty($data[COL_SATUANNAME]) ? $data[COL_SATUANNAME] : '--'?></span></span>
                                </div>
                                <div class="col-sm-8 col-sm-offset-4">
                                    <p class="help-block">
                                        QR Code akan di-generate otomatis sebanyak jumlah diatas setelah data disimpan.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Tujuan</label>
                                <div class="col-sm-8">
                                    <select name="<?=COL_LOCATIONID?>" class="form-control" required>
                                        <option value="">-- Pilih --</option>
                                        <?=GetCombobox("SELECT * FROM mlocation ORDER BY LocationName", COL_LOCATIONID, COL_LOCATIONNAME, (!empty($data[COL_LOCATIONID]) ? $data[COL_LOCATIONID] : null))?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Catatan</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" rows="4" name="<?=COL_REMARKS?>"><?=!empty($data[COL_REMARKS]) ? $data[COL_REMARKS] : ''?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4">Gambar</label>
                                <div class="col-sm-8">
                                    <input type="file" name="userfile[]" accept="image/*" multiple />
                                    <p class="help-block">Opsional, bisa lebih dari 1 file, Maks. 50MB</p>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <?php
                                if(!empty($data[COL_ISSUEID])) {
                                    $files_ = $this->db->where(COL_ISSUEID, $data[COL_ISSUEID])->get(TBL_STOCKISSUEIMAGES)->result_array();
                                    if(count($files_) > 0) {
                                        ?>
                                        <div class="box box-info box-solid">
                                            <div class="box-header with-border">
                                                <h3 class="box-title">GAMBAR / FOTO</h3>

                                                <div class="box-tools pull-right">
                                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="box-body">
                                                <?php
                                                foreach($files_ as $f) {
                                                    ?>
                                                <a href="<?=MY_UPLOADURL.$f[COL_FILENAME]?>" target="_blank">
                                                    <img data-toggle="modal" src="<?=MY_UPLOADURL.$f[COL_FILENAME]?>" style="max-height: 20vh; border: 1.5px solid #dedede; box-shadow: 3px 4px 5px rgba(0,0,0,0.2); margin: 10px" />
                                                </a>
                                                <?php
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    <?php
                                    }
                                    $items_ = $this->db->where(COL_ISSUEID, $data[COL_ISSUEID])->get(TBL_STOCKISSUEITEMS)->result_array();
                                    if($edit && count($items_) > 0) {
                                        ?>
                                        <div class="box box-success box-solid collapsed-box">
                                            <div class="box-header with-border">
                                                <h3 class="box-title">QR CODE</h3>

                                                <div class="box-tools pull-right">
                                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="box-body">
                                                <?php
                                                foreach($items_ as $f) {
                                                    ?>
                                                    <a href="<?=site_url('ajax/qrcode/SIMS.'.$data[COL_ISSUEID].'.'.$data[COL_PURCHASEID].'.'.$f[COL_ITEMID])?>" target="_blank" class="btn btn-app" style="height: auto;">
                                                        <p style="font-family: monospace; font-weight: bold; margin: 0px"><?='SIMS.'.$data[COL_ISSUEID].'.'.$data[COL_PURCHASEID].'.'.$f[COL_ITEMID]?></p>
                                                        <img src="<?=site_url('ajax/qrcode/SIMS.'.$data[COL_ISSUEID].'.'.$data[COL_PURCHASEID].'.'.$f[COL_ITEMID])?>" style="border: 1.5px solid #dedede; box-shadow: 3px 4px 5px rgba(0,0,0,0.2); margin: 10px" />
                                                    </a>
                                                <?php
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    <?php
                                    }
                                } ?>
                            </div>
                        </div>
                        <hr />
                        <div class="col-sm-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-flat pull-right">Simpan</button>
                            </div>
                        </div>
                        <?=form_close()?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" id="browsePurchase" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

<?php $this->load->view('loadjs') ?>
    <script>
        $(document).ready(function() {
            $("[name=<?=COL_STOCKICON?>]").change(function(){
                var val = $(this).val();
                var disp = "<span><img src='"+'<?=MY_IMAGEURL."marker/"?>'+val+"' style='width: 18px' /></span>";
                if(val) {
                    $(this).closest(".input-group").find(".input-group-addon").html(disp);
                }else {
                    $(this).closest(".input-group").find(".input-group-addon").html('<i class="fa fa-map-marker"></i>');
                }
            }).change();

            <?php
            if($edit) {
            ?>
            $('input, button, select, textarea').not('.btn-box-tool').attr('disabled', true);
            <?php
            }
            ?>

            $('#browsePurchase').on('show.bs.modal', function (event) {
                var modalBody = $(".modal-body", $("#browsePurchase"));
                $(this).removeData('bs.modal');

                var departmentID = $("[name=DepartmentID]").val();
                var locationID = $("[name=OriginID]").val();
                var stockID = $("[name=StockID]").val();

                if(!departmentID || !locationID || !stockID) {
                    modalBody.html("<p style='font-style: italic'>Silakan pilih instansi, lokasi dan barang terlebih dahulu!</p>");
                    return;
                }

                modalBody.html("<p style='font-style: italic'>Loading..</p>");
                modalBody.load("<?=site_url("ajax/browse-purchase")?>"+"?DepartmentID="+departmentID+"&LocationID="+locationID+"&StockID="+stockID, function () {
                    $("[name=selIDPurchase][type=hidden]", modalBody).unbind().change(function () {
                        var purchaseID = $(this).val();
                        var issueID = $("[name=selIDIssue][type=hidden]", modalBody).val();
                        var itemID = $("[name=selIDItem][type=hidden]", modalBody).val();
                        $("[name=IssueID]").val(issueID).change();
                        $("[name=ItemID]").val(itemID).change();
                        $("[name=PurchaseID]").val(purchaseID).change();

                        if(issueID && itemID) {
                            $("[name=IssueQty]").val(1).attr("readonly", true);
                            $("[name=ItemNo]").val(purchaseID+'.'+issueID+'.'+itemID);
                            $("[name=ItemNo]").closest('.form-group').show();
                        } else {
                            $("[name=IssueQty]").val(0).attr("readonly", false);
                            $("[name=ItemNo]").val('');
                            $("[name=ItemNo]").closest('.form-group').hide();
                        }
                    });
                    $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                        $("[name=PurchaseNo]").val($(this).val()).change();
                    });
                    /*$("[name=selStockLeft][type=hidden]", modalBody).unbind().change(function () {
                        $("#label-stock-left").html($(this).val());
                    });*/
                });
            });

            $("[name=PurchaseID]").change(function() {
                var departmentID = $("[name=DepartmentID]").val();
                var locationID = $("[name=OriginID]").val();
                var stockID = $("[name=StockID]").val();
                var purchaseID = $("[name=PurchaseID]").val();
                var issueID = $("[name=IssueID]").val();
                var itemID = $("[name=ItemID]").val();

                if(departmentID && locationID && stockID && purchaseID) {
                    $("#label-stock-left").html('<i class="fa fa-spinner fa-spin"></i>');
                    $.get("<?=site_url("ajax/get-stock-left")?>"+"?DepartmentID="+departmentID+"&LocationID="+locationID+"&StockID="+stockID+"&PurchaseID="+purchaseID+"&IssueID="+issueID+"&ItemID="+itemID, function(dat) {
                        var data = JSON.parse(dat);
                        if(data) {
                            $("#label-stock-left").html(desimal(data.QtyLeft,0));
                        }
                        else {
                            $("#label-stock-left").html('0');
                        }
                    }).fail(function() {
                        $("#label-stock-left").html('0');
                    })
                }

            }).change();

            $("[name=OriginID], [name=DepartmentID], [name=StockID]").change(function() {
                var purchaseID = $("[name=PurchaseID]").val();
                if(purchaseID != '') {
                    $("[name=PurchaseID], [name=PurchaseNo]").val('');
                }
            });

            $("[name=<?=COL_STOCKID?>]").change(function(){
                var stockID = $(this).val();
                if(stockID) {
                    $(".satuan").html('<i class="fa fa-spinner fa-spin"></i>');
                    $.get("<?=site_url("ajax/get-stock-satuan")?>", {StockID: stockID}, function(res) {
                        var dat = JSON.parse(res);
                        if(dat && dat.SatuanName) {
                            $(".satuan").html(dat.SatuanName);
                        } else {
                            $(".satuan").html('--');
                        }
                    }).fail(function() {
                        $(".satuan").html('--');
                        console.log('Satuan error.');
                    })
                }
            });
        });
    </script>
<?php $this->load->view('footer') ?>