<?php
/**
 * Created by PhpStorm.
 * User: Toshiba
 * Date: 6/18/2019
 * Time: 11:24 AM
 */
$data = array();
$i = 0;
foreach ($res as $d) {
    $res[$i] = array(
        '<input type="checkbox" class="cekbox" name="cekbox[]" value="' . $d[COL_TRANSFERID] . '" />',
        date('Y-m-d', strtotime($d[COL_TRANSFERDATE])).'<span style="display:none">'.$d[COL_TRANSFERID].'</span>',
        '<strong>'.$d[COL_STOCKNAME].'</strong><br /><small>'.$d[COL_DEPARTMENTNAME].'</small>',
        $d[COL_PURCHASENO].'<br /><small>SIMS.'.$d[COL_PURCHASEID].'.'.$d[COL_ISSUEID].'.'.$d[COL_ITEMID].'</small>',
        $d['OriginName'],
        $d[COL_LOCATIONNAME],
        anchor('log/transfer-view/'.$d[COL_TRANSFERID],'<i class="fa fa-info-circle" title="Lihat Detail"></i>', array('class'=>'btn btn-primary btn-xs btn-flat')).'&nbsp'.anchor('log/stock-track/'.$d[COL_ISSUEID],'<i class="fa fa-search" title="Tracking"></i>', array('class'=>'btn btn-warning btn-xs btn-flat'))
    );
    $i++;
}
$data = json_encode($res);
$user = GetLoggedUser();
?>

<?php $this->load->view('header')
?>
    <section class="content-header">
        <h1><?= $title ?>  <small>Data</small></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a>
            </li>
            <li class="active">
                Mutasi Barang
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <p>
            <?=anchor('log/transfer-delete','<i class="fa fa-trash-o"></i> Hapus',array('class'=>'cekboxaction btn btn-danger btn-sm','confirm'=>'Apa anda yakin?'))?>
            <?=anchor('log/transfer-add','<i class="fa fa-plus"></i> Data Baru',array('class'=>'btn btn-primary btn-sm'))?>
        </p>
        <div class="box box-default">
            <div class="box-body">
                <form id="dataform" method="post" action="#">
                    <table id="datalist" class="table table-bordered table-hover">

                    </table>
                </form>
            </div>
        </div>
    </section>

<?php $this->load->view('loadjs')?>
    <script type="text/javascript">
        $(document).ready(function() {
            var dataTable = $('#datalist').dataTable({
                //"sDom": "Rlfrtip",
                "aaData": <?=$data?>,
                //"bJQueryUI": true,
                //"aaSorting" : [[5,'desc']],
                "scrollY" : '40vh',
                "scrollX": "120%",
                "iDisplayLength": 100,
                "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
                "dom":"R<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
                "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
                "order": [[ 1, "desc" ]],
                "aoColumns": [
                    {"sTitle": "<input type=\"checkbox\" id=\"cekbox\" class=\"\" />",bSortable:false, "width": "10px"},
                    {"sTitle": "Tanggal"},
                    {"sTitle": "Barang / Instansi"},
                    {"sTitle": "No. Pengadaan"},
                    {"sTitle": "Asal"},
                    {"sTitle": "Tujuan"},
                    {"sTitle": "Opsi",bSortable:false, "width": "50px"}
                ]
            });
            $('#cekbox').click(function(){
                if($(this).is(':checked')){
                    $('.cekbox').prop('checked',true);
                    console.log('clicked');
                }else{
                    $('.cekbox').prop('checked',false);
                }
            });
        });
    </script>

<?php $this->load->view('footer')
?>