<?php
/**
 * Created by PhpStorm.
 * User: Toshiba
 * Date: 6/15/2019
 * Time: 6:08 AM
 */
$this->load->view('header') ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?= $title ?> <small> Form</small></h1>
        <ol class="breadcrumb">
            <li><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?=site_url('log/receipt')?>"> Barang Masuk</a></li>
            <li class="active"><?=$edit?'Edit':'Add'?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="box box-primary" style="border-top-color: transparent">
                    <div class="box-body">
                        <?php if(validation_errors()){ ?>
                            <div class="alert alert-danger">
                                <i class="fa fa-ban"></i> PESAN ERROR :
                                <ul>
                                    <?= validation_errors() ?>
                                </ul>
                            </div>
                        <?php } ?>

                        <?php if(!empty($errormess)){ ?>
                            <div class="alert alert-danger">
                                <i class="fa fa-ban"></i> PESAN ERROR :
                                <?= $errormess ?>
                            </div>
                        <?php } ?>

                        <?php  if($this->input->get('success')){ ?>
                            <div class="form-group alert alert-success alert-dismissible">
                                <i class="fa fa-check"></i>
                                Berhasil.
                            </div>
                        <?php } ?>

                        <?php  if($this->input->get('error')){ ?>
                            <div class="form-group alert alert-danger alert-dismissible">
                                <i class="fa fa-ban"></i>
                                Gagal mengupdate data, silahkan coba kembali
                            </div>
                        <?php } ?>

                        <?=form_open_multipart(current_url(),array('role'=>'form','id'=>'stock-form','class'=>'form-horizontal'))?>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Instansi</label>
                                <div class="col-sm-8">
                                    <select name="<?=COL_DEPARTMENTID?>" class="form-control" required>
                                        <?=GetCombobox("SELECT * FROM mdepartment ORDER BY DepartmentName", COL_DEPARTMENTID, COL_DEPARTMENTNAME, (!empty($data[COL_DEPARTMENTID]) ? $data[COL_DEPARTMENTID] : null))?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Nama Barang</label>
                                <div class="col-sm-8">
                                    <select name="<?=COL_STOCKID?>" class="form-control" required>
                                        <option value="">-- Pilih --</option>
                                        <?=GetCombobox("SELECT * FROM mstock ORDER BY StockName", COL_STOCKID, COL_STOCKNAME, (!empty($data[COL_STOCKID]) ? $data[COL_STOCKID] : null))?>
                                    </select>
                                </div>
                            </div>
                            <?php
                            if($edit) {
                              $year = 0;
                              if(!empty($data[COL_PURCHASEDATE])) {
                                $year = date('Y', strtotime($data[COL_PURCHASEDATE]));
                              }
                              $purchaseNo = str_pad($data[COL_CATEGORYID], 3, "0", STR_PAD_LEFT).".".str_pad($data[COL_STOCKID], 3, "0", STR_PAD_LEFT).".".str_pad($data[COL_PURCHASEID], 4, "0", STR_PAD_LEFT).".".$year;
                              ?>
                              <div class="form-group">
                                  <label  class="control-label col-sm-4">No. Pengadaan</label>
                                  <div class="col-sm-8">
                                      <input type="text" class="form-control" value="<?=!empty($purchaseNo) ? $purchaseNo : ''?>" required />
                                  </div>
                              </div>
                              <?php
                            }
                             ?>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Tanggal</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control datepicker" name="<?=COL_PURCHASEDATE?>" value="<?=!empty($data[COL_PURCHASEDATE]) ? $data[COL_PURCHASEDATE] : ''?>" required />
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Lokasi</label>
                                <div class="col-sm-8">
                                    <select name="<?=COL_LOCATIONID?>" class="form-control" required>
                                        <option value="">-- Pilih --</option>
                                        <?=GetCombobox("SELECT * FROM mlocation ORDER BY LocationName", COL_LOCATIONID, COL_LOCATIONNAME, (!empty($data[COL_LOCATIONID]) ? $data[COL_LOCATIONID] : null))?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Harga Satuan</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control uang text-right" name="<?=COL_PRICE?>" value="<?=!empty($data[COL_PRICE]) ? $data[COL_PRICE] : ''?>" required />
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Jumlah</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control uang text-right" name="<?=COL_PURCHASEQTY?>" value="<?=!empty($data[COL_PURCHASEQTY]) ? $data[COL_PURCHASEQTY] : ''?>" required />
                                </div>
                                <div class="col-sm-4">
                                    <span id="satuan" class="control-label" style="float: left;"><span class="satuan"><?=!empty($data[COL_SATUANNAME]) ? $data[COL_SATUANNAME] : '--'?></span></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Umur (bulan)</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control uang text-right" name="<?=COL_LIFE?>" value="<?=!empty($data[COL_LIFE]) ? $data[COL_LIFE] : ''?>" required />
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Tahun Anggaran</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="<?=COL_BUDGETYEAR?>" value="<?=!empty($data[COL_BUDGETYEAR]) ? $data[COL_BUDGETYEAR] : ''?>" required />
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Sumber Dana</label>
                                <div class="col-sm-8">
                                    <select name="<?=COL_FUNDID?>" class="form-control" required>
                                        <?=GetCombobox("SELECT * FROM mfunds ORDER BY FundName", COL_FUNDID, COL_FUNDNAME, (!empty($data[COL_FUNDID]) ? $data[COL_FUNDID] : null))?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Kondisi</label>
                                <div class="col-sm-8">
                                    <select name="<?=COL_CONDITION?>" class="form-control" required>
                                        <?=GetCombobox("SELECT * FROM mcondition ORDER BY ConditionName", COL_CONDITIONID, COL_CONDITIONNAME, (!empty($data[COL_CONDITION]) ? $data[COL_CONDITION] : null))?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Catatan</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" rows="4" name="<?=COL_REMARKS?>"><?=!empty($data[COL_REMARKS]) ? $data[COL_REMARKS] : ''?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4">Gambar</label>
                                <div class="col-sm-8">
                                    <input type="file" name="userfile[]" accept="image/*" multiple />
                                    <p class="help-block">Opsional, bisa lebih dari 1 file, Maks. 50MB</p>
                                </div>
                                <div class="clearfix"></div>
                                <?php
                                if(!empty($data[COL_PURCHASEID])) {
                                    $files_ = $this->db->where(COL_PURCHASEID, $data[COL_PURCHASEID])->get(TBL_STOCKPURCHASEIMAGES)->result_array();
                                    ?>
                                    <hr />
                                    <div style="margin: 5px 0px">
                                        <?php
                                        foreach($files_ as $f) {
                                            ?>
                                            <img src="<?=MY_UPLOADURL.$f[COL_FILENAME]?>" style="max-height: 20vh; border: 1.5px solid #dedede; box-shadow: 3px 4px 5px rgba(0,0,0,0.2); margin: 10px" />
                                        <?php
                                        }
                                        ?>
                                    </div>
                                    <br />
                                <?php } ?>

                            </div>
                        </div>
                        <div class="clearfix"></div><hr />
                        <div class="col-sm-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-flat pull-right">Simpan</button>
                            </div>
                        </div>
                        <?=form_close()?>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php $this->load->view('loadjs') ?>
    <script>
        $(document).ready(function() {
            $("[name=<?=COL_STOCKICON?>]").change(function(){
                var val = $(this).val();
                var disp = "<span><img src='"+'<?=MY_IMAGEURL."marker/"?>'+val+"' style='width: 18px' /></span>";
                if(val) {
                    $(this).closest(".input-group").find(".input-group-addon").html(disp);
                }else {
                    $(this).closest(".input-group").find(".input-group-addon").html('<i class="fa fa-map-marker"></i>');
                }
            }).change();

            $("[name=<?=COL_STOCKID?>]").change(function(){
                var stockID = $(this).val();
                if(stockID) {
                    $("#satuan").html('<i class="fa fa-spinner fa-spin"></i>');
                    $.get("<?=site_url("ajax/get-stock-satuan")?>", {StockID: stockID}, function(res) {
                        var dat = JSON.parse(res);
                        if(dat && dat.SatuanName) {
                            $("#satuan").html(dat.SatuanName);
                        } else {
                            $("#satuan").html('--');
                        }
                    }).fail(function() {
                        $("#satuan").html('--');
                        console.log('Satuan error.');
                    })
                }
            });

            <?php
            if($edit) {
            ?>
            $('input, button, select, textarea').attr('disabled', true);
            <?php
            }
            ?>
        });
    </script>
<?php $this->load->view('footer') ?>
