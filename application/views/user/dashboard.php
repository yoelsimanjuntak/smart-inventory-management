<?php $this->load->view('header') ?>
<!-- Content Header (Page header) -->
<style>
    /* Set the size of the div element that contains the map */
    #map {
        height: 64vh;  /* The height is 400 pixels */
        width: 100%;  /* The width is the width of the web page */
    }
    #map td {
        padding: 2px;
    }
</style>
<section class="content-header" style="box-shadow: 1px 1px 2px #dd4b39">
    <h1>
        Dashboard <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>
<section class="content">
    <div class="col-sm-8">
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Persebaran Lokasi Barang Keluar</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <p style="font-style: italic"><span class="text-red">*)</span> Klik pada marker peta untuk melihat informasi detail</p>
                <div id="map"></div>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Tautan</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-8">
                        <div class="chart-responsive">
                            <canvas id="pieChart" height="155" width="205" style="width: 205px; height: 155px;"></canvas>
                        </div>
                        <!-- ./chart-responsive -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-4">
                        <ul class="chart-legend clearfix">
                            <?php
                            foreach($postcategories as $cat) {
                                ?>
                                <li><i class="fa fa-circle-o" style="color: <?=$cat[COL_POSTCATEGORYLABEL]?>;"></i> <?=$cat[COL_POSTCATEGORYNAME]?></li>
                            <?php
                            }
                            ?>
                        </ul>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.box-body -->
        </div>
    </div>
    <div class="clearfix"></div>
</section>
<?php $this->load->view('loadjs')?>
<script>
    var map; //Will contain map object.
    var marker = false; ////Has the user plotted their location marker?
    var gis;

    //Function called to initialize / create the map.
    //This is called when the page has loaded.
    function initMap() {
        //The center location of our map.
        var centerOfMap = new google.maps.LatLng(2.2587974,98.7436999);
        var infowindow = new google.maps.InfoWindow();

        //Map options.
        var options = {
            center: centerOfMap,
            zoom: 13
        };

        //Create the map object.
        map = new google.maps.Map(document.getElementById('map'), options);
        /*var currLocation = new google.maps.Marker({
            position: centerOfMap,
            map: map,
            title: 'Lokasi sekarang',
            icon: {
                url: "http://maps.google.com/mapfiles/ms/icons/red-dot.png"
            }
        });*/
        $.get("<?=site_url("ajax/get-stock-distribution")?>", function(dat) {
            var data = JSON.parse(dat);
            if(data) {
                for(var i = 0; i < data.length; i++){
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(parseFloat(data[i].Latitude), data[i].Longitude),
                        map: map,
                        /*icon: {
                         url: '<?=MY_IMAGEURL."marker/"?>'+data[i].StockIcon
                         },*/
                    });
                    gis = data[i].LastLocationID;

                    /*gis = '<h4>'+data[i].LastLocationName+'</h4>';
                     gis += '<table>';
                     gis += '<tr><td>Instansi</td><td>:</td><td><b>'+data[i].DepartmentName+'</b></td></tr>';
                     gis += '<tr><td>Kategori</td><td>:</td><td><b>'+data[i].CategoryName+'</b></td></tr>';
                     gis += '<tr><td>Barang</td><td>:</td><td><b>'+data[i].StockName+'</b></td></tr>';
                     gis += '<tr><td>No. Tracking</td><td>:</td><td><b>'+data[i].PurchaseNo+'</b></td></tr>';
                     gis += '<tr><td>No. Item</td><td>:</td><td><b>'+data[i].ItemID+'</b></td></tr>';
                     gis += '<tr><td>Jlh</td><td>:</td><td><b>'+desimal(data[i].Qty,0)+'</b></td></tr>';
                     gis += '<tr><td>Kondisi</td><td>:</td><td><b>'+data[i].LastConditionName+'</b></td></tr>';
                     gis += '</table>';*/


                    google.maps.event.addListener(marker, 'click', (function(marker,gis) {
                        return function(){
                            $.get("<?=site_url("ajax/get-stock-by-location")?>", {LocationID: gis}, function(dat) {
                                infowindow.setContent(dat);
                                infowindow.open(map, marker);
                            });
                        }
                    })
                    (marker,gis));
                }
            }
            else {

            }
        }).fail(function() {
            console.log('Gagal menampilkan penanda lokasi.');
        });
    }

    //-------------
    //- PIE CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
    var pieChart = new Chart(pieChartCanvas);
    var PieData = <?=json_encode($posts)?>;
    var pieOptions = {
        //Boolean - Whether we should show a stroke on each segment
        segmentShowStroke: true,
        //String - The colour of each segment stroke
        segmentStrokeColor: "#fff",
        //Number - The width of each segment stroke
        segmentStrokeWidth: 1,
        //Number - The percentage of the chart that we cut out of the middle
        percentageInnerCutout: 50, // This is 0 for Pie charts
        //Number - Amount of animation steps
        animationSteps: 100,
        //String - Animation easing effect
        animationEasing: "easeOutBounce",
        //Boolean - Whether we animate the rotation of the Doughnut
        animateRotate: true,
        //Boolean - Whether we animate scaling the Doughnut from the centre
        animateScale: false,
        //Boolean - whether to make the chart responsive to window resizing
        responsive: true,
        // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
        maintainAspectRatio: false,
        //String - A legend template
        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
        //String - A tooltip template
        tooltipTemplate: "<%=value %> <%=label%> users"
    };
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    pieChart.Doughnut(PieData, pieOptions);
    //-----------------
    //- END PIE CHART -
    //-----------------
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDzvhEg4efwXcYvdMvRLDGUEau4vjh8klg&callback=initMap">
</script>
<?php $this->load->view('footer') ?>
