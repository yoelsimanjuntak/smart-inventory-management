<?php
/**
 * Created by PhpStorm.
 * User: Toshiba
 * Date: 6/14/2019
 * Time: 8:22 AM
 */
$this->load->view('header') ?>
    <style>
        /* Set the size of the div element that contains the map */
        #map {
            height: 400px;  /* The height is 400 pixels */
            width: 100%;  /* The width is the width of the web page */
        }
    </style>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?= $title ?> <small> Form</small></h1>
        <ol class="breadcrumb">
            <li><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?=site_url('master/location-index')?>"> Lokasi</a></li>
            <li class="active"><?=$edit?'Edit':'Add'?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="box box-primary" style="border-top-color: transparent">
                    <div class="box-body">
                        <?php if(validation_errors()){ ?>
                            <div class="alert alert-danger alert-dismissible">
                                <i class="fa fa-ban"></i>
                                <?= validation_errors() ?>
                            </div>
                        <?php } ?>

                        <?php  if($this->input->get('success')){ ?>
                            <div class="form-group alert alert-success alert-dismissible">
                                <i class="fa fa-check"></i>
                                Berhasil.
                            </div>
                        <?php } ?>

                        <?php  if($this->input->get('error')){ ?>
                            <div class="form-group alert alert-danger alert-dismissible">
                                <i class="fa fa-ban"></i>
                                Gagal mengupdate data, silahkan coba kembali
                            </div>
                        <?php }
                        if(!empty($upload_errors)) {
                            ?>
                            <div class="alert alert-danger alert-dismissible">
                                <i class="fa fa-ban"></i>
                                <?=$upload_errors?>
                            </div>
                        <?php
                        }
                        ?>

                        <?=form_open(current_url(),array('role'=>'form','id'=>'post','class'=>'form-horizontal'))?>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Nama</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="<?=COL_LOCATIONNAME?>" value="<?=!empty($data[COL_LOCATIONNAME]) ? $data[COL_LOCATIONNAME] : ''?>" required />
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Deskripsi</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" rows="4" name="<?=COL_DESCRIPTION?>"><?=!empty($data[COL_DESCRIPTION]) ? $data[COL_DESCRIPTION] : ''?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Latitude</label>
                                <div class="col-sm-8">
                                    <input type="text" id="lat" class="form-control" name="<?=COL_LATITUDE?>" value="<?=!empty($data[COL_LATITUDE]) ? $data[COL_LATITUDE] : ''?>" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Longitude</label>
                                <div class="col-sm-8">
                                    <input type="text" id="lng" class="form-control" name="<?=COL_LONGITUDE?>" value="<?=!empty($data[COL_LONGITUDE]) ? $data[COL_LONGITUDE] : ''?>" />
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <p class="font-italic"><span class="text-red">*)</span> Klik pada map untuk menandai lokasi</p>
                            <div id="map"></div>
                        </div>
                        <div class="clearfix"></div><hr />
                        <div class="col-sm-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-flat pull-right">Simpan</button>
                            </div>
                        </div>
                        <?=form_close()?>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php $this->load->view('loadjs') ?>
    <script>
        var map; //Will contain map object.
        var marker = false; ////Has the user plotted their location marker?

        //Function called to initialize / create the map.
        //This is called when the page has loaded.
        function initMap() {
            var currLat = $("#lat").val();
            var currLng = $("#lng").val();

            //The center location of our map.
            var centerOfMap = new google.maps.LatLng(2.2587974,98.7436999);
            if(currLat && currLng) {
                centerOfMap = new google.maps.LatLng(parseFloat(currLat), parseFloat(currLng));
            }

            //Map options.
            var options = {
                center: centerOfMap, //Set center.
                zoom: 14 //The zoom value.
            };

            //Create the map object.
            map = new google.maps.Map(document.getElementById('map'), options);

            if(currLat && currLng) {
                var currLatLng = {lat: parseFloat(currLat),lng: parseFloat(currLng)};
                var currLocation = new google.maps.Marker({
                    position: currLatLng,
                    map: map,
                    title: 'Lokasi sekarang',
                    icon: {
                        url: "http://maps.google.com/mapfiles/ms/icons/red-dot.png"
                    }
                });
            }


            //Listen for any clicks on the map.
            google.maps.event.addListener(map, 'click', function(event) {
                //Get the location that the user clicked.
                var clickedLocation = event.latLng;
                //If the marker hasn't been added.
                if(marker === false){
                    //Create the marker.
                    marker = new google.maps.Marker({
                        position: clickedLocation,
                        map: map,
                        draggable: true //make it draggable
                    });
                    //Listen for drag events!
                    google.maps.event.addListener(marker, 'dragend', function(event){
                        markerLocation();
                    });
                } else{
                    //Marker has already been added, so just change its location.
                    marker.setPosition(clickedLocation);
                }
                //Get the marker's location.
                markerLocation();
            });
        }

        //This function will get the marker's current location and then add the lat/long
        //values to our textfields so that we can save the location.
        function markerLocation(){
            //Get location.
            var currentLocation = marker.getPosition();
            //Add lat and lng values to a field that we can save.
            document.getElementById('lat').value = currentLocation.lat(); //latitude
            document.getElementById('lng').value = currentLocation.lng(); //longitude
        }

        $('[name=Latitude], [name=Longitude]').change(function() {
            var lat = $('[name=Latitude]').val();
            var long = $('[name=Longitude]').val();

            if(lat && long) {
                var clickedLocation = {lat: parseFloat(lat),lng: parseFloat(long)};
                if(marker === false){
                    marker = new google.maps.Marker({
                        position: clickedLocation,
                        map: map,
                        draggable: true
                    });
                    google.maps.event.addListener(marker, 'dragend', function(event){
                        markerLocation();
                    });
                } else{
                    marker.setPosition(clickedLocation);
                }
                map.setCenter(clickedLocation);
            }
        });
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDzvhEg4efwXcYvdMvRLDGUEau4vjh8klg&callback=initMap">
    </script>
<?php $this->load->view('footer') ?>