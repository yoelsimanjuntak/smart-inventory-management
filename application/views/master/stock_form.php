<?php
/**
 * Created by PhpStorm.
 * User: Toshiba
 * Date: 6/15/2019
 * Time: 12:45 AM
 */
$this->load->view('header') ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?= $title ?> <small> Form</small></h1>
        <ol class="breadcrumb">
            <li><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?=site_url('master/stock-index')?>"> Barang</a></li>
            <li class="active"><?=$edit?'Edit':'Add'?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="box box-primary" style="border-top-color: transparent">
                    <div class="box-body">
                        <?php if(validation_errors()){ ?>
                            <div class="alert alert-danger alert-dismissible">
                                <i class="fa fa-ban"></i>
                                <?= validation_errors() ?>
                            </div>
                        <?php } ?>

                        <?php  if($this->input->get('success')){ ?>
                            <div class="form-group alert alert-success alert-dismissible">
                                <i class="fa fa-check"></i>
                                Berhasil.
                            </div>
                        <?php } ?>

                        <?php  if($this->input->get('error')){ ?>
                            <div class="form-group alert alert-danger alert-dismissible">
                                <i class="fa fa-ban"></i>
                                Gagal mengupdate data, silahkan coba kembali
                            </div>
                        <?php } ?>

                        <?=form_open(current_url(),array('role'=>'form','id'=>'stock-form','class'=>'form-horizontal'))?>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Nama</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="<?=COL_STOCKNAME?>" value="<?=!empty($data[COL_STOCKNAME]) ? $data[COL_STOCKNAME] : ''?>" required />
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Merk</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="<?=COL_STOCKBRAND?>" value="<?=!empty($data[COL_STOCKBRAND]) ? $data[COL_STOCKBRAND] : ''?>" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Satuan</label>
                                <div class="col-sm-8">
                                    <select name="<?=COL_SATUANID?>" class="form-control" required>
                                        <option value="">Pilih Satuan</option>
                                        <?=GetCombobox("SELECT * FROM msatuan ORDER BY SatuanName", COL_SATUANID, COL_SATUANNAME, (!empty($data[COL_SATUANID]) ? $data[COL_SATUANID] : null))?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Kategori</label>
                                <div class="col-sm-8">
                                    <select name="<?=COL_CATEGORYID?>" class="form-control" required>
                                        <option value="">Pilih Kategori</option>
                                        <?=GetCombobox("SELECT * FROM mcategory ORDER BY CategoryName", COL_CATEGORYID, COL_CATEGORYNAME, (!empty($data[COL_CATEGORYID]) ? $data[COL_CATEGORYID] : null))?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Icon</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                        </div>
                                        <select name="<?=COL_STOCKICON?>" class="no-select2 form-control" required>
                                            <option value="">Pilih Icon</option>
                                            <?=GetComboboxFile(MY_IMAGEPATH."marker/", (!empty($data[COL_STOCKICON]) ? $data[COL_STOCKICON] : null))?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Deskripsi</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" rows="4" name="<?=COL_STOCKDESCRIPTION?>"><?=!empty($data[COL_STOCKDESCRIPTION]) ? $data[COL_STOCKDESCRIPTION] : ''?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div><hr />
                        <div class="col-sm-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-flat pull-right">Simpan</button>
                            </div>
                        </div>
                        <?=form_close()?>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php $this->load->view('loadjs') ?>
<script>
    $(document).ready(function() {
        $("[name=<?=COL_STOCKICON?>]").change(function(){
            var val = $(this).val();
            var disp = "<span><img src='"+'<?=MY_IMAGEURL."marker/"?>'+val+"' style='width: 18px' /></span>";
            if(val) {
                $(this).closest(".input-group").find(".input-group-addon").html(disp);
            }else {
                $(this).closest(".input-group").find(".input-group-addon").html('<i class="fa fa-map-marker"></i>');
            }
        }).change();
    });
</script>
<?php $this->load->view('footer') ?>